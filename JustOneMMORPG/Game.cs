using System;
using System.Collections.Generic;
using JustOneMMORPG.gameplay;
using JustOneMMORPG.graphics;
using JustOneMMORPG.sound;
using OpenTK;
using OpenTK.Audio.OpenAL;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace JustOneMMORPG
{
    public class Game : GameWindow
    {
        public static double GlobalTime {get; private set; }
        public static Matrix4 Projection {get; private set; }
        public static Matrix4 OrthographicProjection {get; private set; }

        public static Vector2 WindowSize {get; private set;}
        public static Vector3 cameraPos {get; private set;} = new Vector3(-8, 8, -8);

        public static int FPS {get; private set;}
        public static float LastFPSCheck {get; private set;}
        public static float FramesCount {get; private set;}

        public static bool GameFocused {get; private set;} = true;

        public static Camera Camera {get; private set;} = new Camera(new Vector3(0, 4, 0));

        public SoundContainer Ambient;

        private SoundManager SoundMng = new SoundManager();

        private List<Drawable> Stones = new List<Drawable>();


        public Game(int width, int height, string title) : base(width, height, GraphicsMode.Default, title, GameWindowFlags.Default, DisplayDevice.Default, 3, 3, GraphicsContextFlags.Default) {
            WindowSize = new Vector2(width, height);
        }

        protected override void OnLoad(EventArgs e)
        {
            Ambient = SoundMng.Add(Sound.FromMP3("assets/sounds/znt-say_yes.mp3", ALFormat.Stereo16), new Vector3(0));

            GL.ClearColor(0f, 0f, 0f, 1f);
            GL.Enable(EnableCap.DepthTest);
            GL.Enable(EnableCap.Blend);
            GL.BlendFunc((BlendingFactor)BlendingFactorSrc.SrcAlpha, (BlendingFactor)BlendingFactorDest.OneMinusSrcAlpha);

            UpdateMatrix();


            var storage = new VertexStorage(
                new[]{
                    new Vertex(
                        new[]{
                            -1f, 1f, -1f,
                            -1f, -1f, -1f,
                            1f, -1f, -1f,

                            -1f, 1f, -1f,
                            1f, -1f, -1f,
                            1f, 1f, -1f,


                            -1f, 1f, 1f,
                            -1f, -1f, 1f,
                            1f, -1f, 1f,

                            -1f, 1f, 1f,
                            1f, -1f, 1f,
                            1f, 1f, 1f,
                            

                            1f, 1f, -1f,
                            1f, -1f, -1f,
                            1f, -1f, 1f,

                            1f, 1f, -1f,
                            1f, -1f, 1f,
                            1f, 1f, 1f,
                            

                            -1f, 1f, -1f,
                            -1f, -1f, -1f,
                            -1f, -1f, 1f,

                            -1f, 1f, -1f,
                            -1f, -1f, 1f,
                            -1f, 1f, 1f,
                            

                            1f, 1f, 1f,
                            -1f, 1f, 1f,
                            -1f, 1f, -1f,

                            1f, 1f, 1f,
                            -1f, 1f, -1f,
                            1f, 1f, -1f,
                            

                            1f, -1f, 1f,
                            -1f, -1f, 1f,
                            -1f, -1f, -1f,

                            1f, -1f, 1f,
                            -1f, -1f, -1f,
                            1f, -1f, -1f
                        
                    
                        }, 0, 3
                    ),
                    new Vertex(
                        new[]{
                            0f, 1f,
                            0f, 0f,
                            1f, 0f,

                            0f, 1f,
                            1f, 0f,
                            1f, 1f,

                            
                            0f, 1f,
                            0f, 0f,
                            1f, 0f,

                            0f, 1f,
                            1f, 0f,
                            1f, 1f,

                            
                            0f, 1f,
                            0f, 0f,
                            1f, 0f,

                            0f, 1f,
                            1f, 0f,
                            1f, 1f,

                            
                            0f, 1f,
                            0f, 0f,
                            1f, 0f,

                            0f, 1f,
                            1f, 0f,
                            1f, 1f,

                            
                            0f, 1f,
                            0f, 0f,
                            1f, 0f,

                            0f, 1f,
                            1f, 0f,
                            1f, 1f,

                            
                            0f, 1f,
                            0f, 0f,
                            1f, 0f,

                            0f, 1f,
                            1f, 0f,
                            1f, 1f

                            
                        }, 1, 2),
                    new Vertex(
                        new[]{
                            //Backward
                            0f, 0f, -1f,
                            0f, 0f, -1f,
                            0f, 0f, -1f,
                            0f, 0f, -1f,
                            0f, 0f, -1f,
                            0f, 0f, -1f,

                            //Forward
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            
                            //Right
                            1f, 0f, 0f,
                            1f, 0f, 0f,
                            1f, 0f, 0f,
                            1f, 0f, 0f,
                            1f, 0f, 0f,
                            1f, 0f, 0f,
                            
                            //Left
                            -1f, 0f, 0f,
                            -1f, 0f, 0f,
                            -1f, 0f, 0f,
                            -1f, 0f, 0f,
                            -1f, 0f, 0f,
                            -1f, 0f, 0f,
                            
                            //Top
                            0f, 1f, 0f,
                            0f, 1f, 0f,
                            0f, 1f, 0f,
                            0f, 1f, 0f,
                            0f, 1f, 0f,
                            0f, 1f, 0f,
                            
                            //Bottom
                            0f, -1f, 0f,
                            0f, -1f, 0f,
                            0f, -1f, 0f,
                            0f, -1f, 0f,
                            0f, -1f, 0f,
                            0f, -1f, 0f

                            
                        }, 2, 3
                    )
                }
            );

            var stoneTexture = new Texture("assets/textures/stone.png");

            for(var i=0;i<11; i++){
                for(var j=0;j<11; j++){
                    var stone = new Drawable(storage);
                    stone.Texture = stoneTexture;
                    stone.Position = new Vector3(5*2-i*2, 0, 5*2-j*2);

                    Stones.Add(stone);
                }
            }

            base.OnLoad(e);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GlobalTime += e.Time;

            if(GlobalTime - LastFPSCheck > 1){
                FPS = (int)FramesCount;
                LastFPSCheck = (float)GlobalTime;
                FramesCount = 0;
            }else{
                FramesCount++;
            }


            var input = Keyboard.GetState();


            if (input.IsKeyDown(Key.Escape))
            {
                Exit();
            }

            if(input.IsKeyDown(Key.LControl))
                GameFocused = false;
            
            if(input.IsKeyDown(Key.RControl))
                GameFocused = true;

            Camera.KeyHandler(input, e.Time);


            Title = "Just One MMORPG > fps: "+FPS+", Pos: "+Camera.Position;

            SoundMng.PlayTick(cameraPos, Camera.Front, new Vector3(0), Vector3.UnitY);

            //Matrix4 view = Matrix4.CreateTranslation(0.0f, 0.0f, -3.0f);
            Assets.Shaders["default"].SetMatrix4("projection", Projection);
            Assets.Shaders["default"].SetFloat("global_time", (float)GlobalTime);
            Assets.Shaders["default"].SetMatrix4("view", Camera.GetViewMatrix());
            Assets.Shaders["default"].SetVector3("viewPos", Camera.Position);
            Assets.Shaders["default"].SetVector3("lightPos", new Vector3(MathF.Cos((float)GlobalTime)*5f, 5, MathF.Sin((float)GlobalTime))*5f);
            Assets.Shaders["default"].SetVector3("lightColor", new Vector3(1));


            foreach(var stone in Stones){
                stone.Draw(Assets.Shaders["default"]);
            }

            Context.SwapBuffers();
            base.OnRenderFrame(e);
        }

        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            if (Focused && GameFocused){
                Camera.Update(Width/2f, Height/2f, e.X, e.Y);
                Mouse.SetPosition(X + Width/2f, Y + Height/2f);

                

                CursorVisible = false;
            }else{
                CursorVisible = true;
            }

            base.OnMouseMove(e);
        }

        private void UpdateMatrix(){
            OrthographicProjection = Matrix4.CreateOrthographicOffCenter(0.0f, Width, 0.0f, Height, 0.1f, 100.0f);
            Projection = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(45.0f), Width / (float)Height, 0.1f, 100.0f);
        }

        protected override void OnResize(EventArgs e)
        {
            WindowSize = new Vector2(Width, Height);
            GL.Viewport(0, 0, Width, Height);

            UpdateMatrix();
            
            base.OnResize(e);
        }
    }
}