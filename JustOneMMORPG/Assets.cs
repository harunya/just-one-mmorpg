using System.Collections.Generic;
using JustOneMMORPG.graphics;

namespace JustOneMMORPG
{
    public static class Assets
    {
        public static Dictionary<string, Shader> Shaders = new Dictionary<string, Shader>();
        static Assets (){
            // TODO: practice shader managing in file
            Shaders.Add("default", new Shader("assets/shaders/shader.vert", "assets/shaders/shader.frag"));
            Shaders.Add("HUD", new Shader("assets/shaders/hud.vert", "assets/shaders/hud.frag"));
            Shaders.Add("progress", new Shader("assets/shaders/hud.vert", "assets/shaders/progress.frag"));
            //Shaders.Add("entity", new Shader("assets/shaders/entity.vert", "assets/shaders/shader.frag"));
        }
    }

}