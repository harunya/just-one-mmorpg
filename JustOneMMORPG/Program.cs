﻿using OpenTK;

namespace JustOneMMORPG
{
    public class Program : GameWindow
    {
        public static void Main() {
            using (var game = new Game(800, 600, "Prototype"))
            {
                game.Run();
            }
        }

    }
}