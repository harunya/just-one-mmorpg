#version 330

out vec4 outputColor;

in vec2 texCoord;

uniform sampler2D texture0;
uniform float global_time;

void main()
{
    vec4 color = texture(texture0, texCoord);
    outputColor = color;

    float gamma = 0.8;
    outputColor.rgb = pow(outputColor.rgb, vec3(1.0/gamma));
}