#version 330 core

layout(location = 0) in vec3 aPosition;

layout(location = 1) in vec2 aTexCoord;

out vec2 texCoord;

uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;
uniform vec3 screen_size;
uniform float origin;

void main(void)
{
    texCoord = aTexCoord;
    gl_Position = projection * model * vec4(aPosition+origin, 1.0);
}