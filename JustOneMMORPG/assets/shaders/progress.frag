#version 330

out vec4 outputColor;

in vec2 texCoord;

uniform vec4 color;
uniform vec4 backgound_color;
uniform vec2 size;
uniform vec2 position;
uniform float border;
uniform vec4 border_color;
uniform float value;

void main()
{
    vec2 pos = gl_FragCoord.xy - position;

    if((pos.y < border || pos.y > size.y - border) || (pos.x < 2 || pos.x > size.x - border))
        outputColor = border_color;
    else if(pos.x < value * size.x)
        outputColor = color;
    else
        outputColor = backgound_color;
}