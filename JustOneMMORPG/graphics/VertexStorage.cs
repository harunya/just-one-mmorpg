namespace JustOneMMORPG.graphics
{
    public class VertexStorage
    {
        public Vertex[] Vertices {get; private set;}
        public int PointsCount {get; private set;}
        public float[] Points {get; private set;}
        public int Sizes {get; private set;}


        public VertexStorage(Vertex[] vertices){
            Vertices = vertices;

            Points = GetArray();
            PointsCount = Points.Length;
        }

        public float[] GetArray(){
            Sizes = 0;
            var Points = 0;

            foreach (var vertex in Vertices)
            {
                vertex.Offset = Sizes;
                Sizes += vertex.Size;
                Points += vertex.Points.Length;
            }

            var arr = new float[Points];
            for(int i=0, k=0;i < arr.Length; k++){
                foreach (var vertex in Vertices)
                {
                    for(var j=0; j < vertex.Size; j++){
                        arr[i++] = vertex.Points[k*vertex.Size+j];
                    }
                }
            }
            

            return arr;
        }
    }
}