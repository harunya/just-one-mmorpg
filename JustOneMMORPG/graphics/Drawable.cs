using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace JustOneMMORPG.graphics
{
    public class Drawable
    {
        private int VBO;
        private int VAO;
        private int VerticesCount;

        public Vector3 Position {get; set;} = Vector3.Zero;
        public Vector3 Rotation {get; set;} = Vector3.Zero;
        public float Scale {get; set;} = 1f;
        public float Origin {get; set;} = 0.5f;

        public Texture Texture {get; set;}


        public Drawable(VertexStorage points){
            VBO = GL.GenBuffer();
            VAO = GL.GenVertexArray();

            Fill(points);
        }

        public void Fill(VertexStorage storage){
            // 1. bind Vertex Array Object
            GL.BindVertexArray(VAO);
            
            // 2. copy our vertices array in a buffer for OpenGL to use
            GL.BindBuffer(BufferTarget.ArrayBuffer, VBO);
            GL.BufferData(BufferTarget.ArrayBuffer, storage.PointsCount * sizeof(float), storage.Points, BufferUsageHint.StaticDraw);
            
            // 3. then set our vertex attributes pointers
            foreach (var vertex in storage.Vertices)
            {
                GL.VertexAttribPointer(
                    vertex.Handle,
                    vertex.Size,
                    VertexAttribPointerType.Float,
                    false,
                    storage.Sizes * sizeof(float),
                    vertex.Offset * sizeof(float)
                );
                
                GL.EnableVertexAttribArray(vertex.Handle);
            }
            
            // GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 5 * sizeof(float), 0);
            // GL.EnableVertexAttribArray(0);
            // GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, 5 * sizeof(float), 3 * sizeof(float));
            // GL.EnableVertexAttribArray(1);


            VerticesCount = storage.PointsCount/storage.Sizes;
        }

        public void Draw(Shader shader){
            shader.Use();
            Texture?.Use();

            var position = Matrix4.CreateTranslation(Position);
            var rotation = Matrix4.CreateRotationX(Rotation.X) * Matrix4.CreateRotationY(Rotation.Y) * Matrix4.CreateRotationZ(Rotation.Z);
            var scaling = Matrix4.CreateScale(Scale);

            shader.SetMatrix4("model", scaling * rotation * position);
            shader.SetVector3("screen_size", new Vector3(Game.WindowSize.X, Game.WindowSize.Y, 0));
            shader.SetFloat("origin", 0f);

            GL.BindVertexArray(VAO);
            GL.DrawArrays(PrimitiveType.Triangles, 0, VerticesCount);
        }

        public static Drawable Create(float x, float y, float z, float w, float h){
            var storage = new VertexStorage(
                new[]{
                    new Vertex(
                        new[]{
                            -w, h, 0,
                            -w, -h, 0,
                            w, -h, 0,

                            -w, h, 0,
                            w, -h, 0,
                            w, h, 0
                    
                        }, 0, 3
                    ),
                    new Vertex(
                        new[]{
                            0f, 1f,
                            0f, 0f,
                            1f, 0f,

                            0f, 1f,
                            1f, 0f,
                            1f, 1f
                            
                        }, 1, 2
                    ),
                    new Vertex(
                        new[]{
                            //Forward
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f
                            
                        }, 2, 3)
                }
            );

            var n = new Drawable(storage);

            n.Position = new Vector3(x, y, z);
            n.Scale = 1/w;

            return n;

        }
    }

}