using OpenTK;
using OpenTK.Audio;
using OpenTK.Audio.OpenAL;

namespace JustOneMMORPG.sound
{
    public class SoundContainer{
        public Sound Sound {get; private set;}
        public float Duration {get; set;}
        public float CreateTime {get; private set;}
        public float Volume {get; set;} = 1f;

        public float Pitch {get; set;} = 1f;
        public Vector3 _position = new Vector3(0);
        public float ReferenceDistance {get; set;} = 0f;
        public float MaxDistance {get; set;} = 10f;

        public bool PositionalSource {get; set;} = false;


        public SoundContainer(Sound sound){
            Sound = sound;
            CreateTime = (float)Game.GlobalTime;
        }
    }

    public class SoundManager
    {
        private SoundContainer[] Slots;
        private AudioContext Context;

        public float Volume {get; set;} = 1f;

        public SoundManager(int max = 128){
            Slots = new SoundContainer[max];

            Context = new AudioContext();
            
            //IntPtr d = Alc.OpenDevice(null);
            //Context = Alc.CreateContext(d, new int[]{0x1992, 1, 0});
        }

        private bool PlayTickSound(SoundContainer container){
            container.Sound.Update();

            AL.Source(container.Sound.Source, ALSourcef.Pitch, container.Pitch);

            if(!container.PositionalSource){
                AL.Source(container.Sound.Source, ALSource3f.Position, ref container._position);
                
                AL.Source(container.Sound.Source, ALSourcef.ReferenceDistance, container.ReferenceDistance);
                AL.Source(container.Sound.Source, ALSourcef.MaxDistance, container.MaxDistance);
            }else{
                AL.Source(container.Sound.Source, ALSourcef.Gain, container.Volume);
            }


            if(container.Duration != 0 && (float)Game.GlobalTime - container.CreateTime > container.Duration){
                AL.SourceStop(container.Sound.Source);
                return false;
            }

            if(container.Sound.State == ALSourceState.Initial || container.Sound.State == ALSourceState.Stopped && container.Duration == 0){
                AL.Source(container.Sound.Source, ALSourcei.Buffer, container.Sound.Buffer);
                AL.SourcePlay(container.Sound.Source);

                return true;
            }

            if(container.Sound.State == ALSourceState.Stopped){ 
                return false;
            }

            return true;
        }

        public void PlayTick(Vector3 pos, Vector3 forw, Vector3 vel, Vector3 up){
            Context.MakeCurrent();

            AL.Listener(ALListener3f.Position, ref pos);
            AL.Listener(ALListenerfv.Orientation, ref forw, ref up);
            //AL.Listener(ALListener3f.Velocity, ref vel);

            for(var i=0;i<Slots.Length;i++)
            {
                var s = Slots[i];

                if(s == null)
                    continue;

                if(!PlayTickSound(s))
                    Slots[i] = null;
            }
        }

        public SoundContainer Add(Sound sound, Vector3 pos, float duration = 0, float volume = 1f){
            for(var i=0;i<Slots.Length;i++){
                if(Slots[i] != null)
                    continue;
                
                Slots[i] = new SoundContainer(sound);
                Slots[i].Duration = duration;
                Slots[i].Volume = volume;
                Slots[i]._position = pos;

                return Slots[i];
            }

            return null;
        }
    }

}