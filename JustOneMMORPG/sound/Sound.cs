using System;
using System.IO;
using MP3Sharp;
using OpenTK.Audio.OpenAL;

namespace JustOneMMORPG.sound
{
    public class Sound
    {
        public byte[] Data {get; set;}
        public Int32 Freq {get; set;} = 44100;
        public ALFormat Type {get; set;} = ALFormat.Stereo16;

        public int Buffer {get; private set;}
        public int Source {get; private set;}

        public ALSourceState State {get; private set;} = ALSourceState.Initial;


        public Sound(byte[] data, Int32 freq = 44100, ALFormat type = ALFormat.Mono16){
            Data = data;
            Freq = freq;
            Type = type;

            Init();
        }

        ~Sound(){
            AL.DeleteSource(Source);
            AL.DeleteBuffer(Buffer);
        }

        private void Init(){
            Buffer = AL.GenBuffer();
            Source = AL.GenSource();

            AL.BufferData(Buffer, Type, Data, Data.Length, Freq);

            
        }

        public void Update(){
            int state;
            AL.GetSource(Source, ALGetSourcei.SourceState, out state);

            State = (ALSourceState)state;
        }

        public static Sound FromMP3(string path, ALFormat type){
            var stream = new MP3Stream(path);

            using var ms = new MemoryStream();
            stream.CopyTo(ms);

            stream.Close();


            Console.WriteLine("Loaded \""+path+"\" [" + ms.Length + " bytes]");


            return new Sound(ms.ToArray(), stream.Frequency, type);
        }
    }

}