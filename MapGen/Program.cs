﻿using System;
using System.IO;
using System.Text;

namespace MapGen
{
    class Program
    {
        private static readonly byte _version = 1;
        static byte[] uintToBytes(uint val)
        {
            byte[] intBytes = BitConverter.GetBytes(val);
            if (!BitConverter.IsLittleEndian)
                Array.Reverse(intBytes);
            return intBytes;
        }
        static void Main(string[] args)
        {
            /*
             *  0-100h - header
             *      0-6 "BACADEF"
             *      7 - Version
             *      8-11 MapWidth
             *      12-14 MapHeight
             *  101h-end - raw
             */
            
            Console.WriteLine("Width = ");
            var width = uint.Parse(Console.ReadLine() ?? string.Empty);
            Console.WriteLine("Height = ");
            var height = uint.Parse(Console.ReadLine() ?? string.Empty);
            
            var bytes = new byte[0x100 + width * height];

            #region Header
            var header = new byte[0x100];
            Array.Copy(Encoding.ASCII.GetBytes("BACADEF"), 0, header, 0, 7);
            header[7] = _version;
            Array.Copy(uintToBytes(width), 0, header, 8, 4);
            Array.Copy(uintToBytes(height), 0, header, 12, 4);

            Array.Copy(header, 0, bytes, 0, 0x100);
            #endregion

            var generator = new Perlin2D();
            for (int i = 0; i < height; ++i)
            {
                for (int j = 0; j < width; ++j)
                {
                    bytes[0x100 + i * width + j] = (byte)(generator.Noise(i, j)*255);
                }
            }
            
            
            File.WriteAllBytes("test.map", bytes);
        }
    }
}