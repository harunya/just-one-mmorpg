﻿using System;
using OpenTK;

namespace SwordAttackProto.code_asbest.Gameplay.Magic
{
    public struct Shape
    {
        // i comeback tomorrow (AppleLoli)
        public static Vector2[] LsStar(float r, Vector2 center, int peakCount, int peakStep)
        {
            var res = new Vector2[peakCount];
            var step = 2 / (float)peakCount * MathF.PI;
            for (var i = 0; i < peakCount; i++)
            {
                var angle = i * peakStep % peakCount * step;
                res[i] = new Vector2(center.X + MathF.Cos(angle) * r, center.Y + MathF.Sin(angle) * r);
            }

            return res;
        }

        public static Vector4[] LRayStar(float r, Vector2 center, int peakCount)
        {
            var res = new Vector4[peakCount];
            var step = 2 / (float)peakCount * MathF.PI;
            for (var i = 0; i < peakCount; i++)
            {
                var angle = i * step;
                res[i] = new Vector4(center.X, center.Y, center.X + MathF.Cos(angle) * r, center.Y + MathF.Sin(angle) * r);
            }

            return res;
        }

        public static Vector2[] LsConvexPolygon(float r, Vector2 center, int peakCount)
        {
            var res = new Vector2[peakCount];
            var step = 2 / (float)peakCount * MathF.PI;
            for (var i = 0; i < peakCount; i++)
            {
                var angle = i * step;
                res[i] = new Vector2(center.X + MathF.Cos(angle) * r, center.Y + MathF.Sin(angle) * r);
            }

            return res;
        }

        public static Vector4[] Ls2L(Vector2[] ls, bool isLoop) // LineStrip to Lines
        {
            var res = new Vector4[isLoop ? ls.Length : ls.Length - 1];
            
            if (isLoop)
                for (var i = 0; i < ls.Length; i++)
                    res[i] = new Vector4(ls[i].X, ls[i].Y, ls[(i + 1) % ls.Length].X, ls[(i + 1) % ls.Length].Y);
            else
                for (var i = 0; i < ls.Length - 1; i++)
                    res[i] = new Vector4(ls[i].X, ls[i].Y, ls[i + 1].X, ls[i+1].Y);
            return res;
        } 
    }
}