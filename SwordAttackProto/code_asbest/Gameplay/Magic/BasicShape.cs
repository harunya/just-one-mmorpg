﻿using OpenTK;
using SwordAttackProto.code_asbest.Common;

namespace SwordAttackProto.code_asbest.Gameplay.Magic
{
    public interface IBasicShape
    {
    }

    public struct Circle : IBasicShape
    {
        public Vector4 Color;

        public Circle(Vector4? color = null)
        {
            Color = color ?? BitmapColor.White;
        }
    }

    public struct ConvexPolygon : IBasicShape
    {
        public Vector4 Color;
        public int PeakCount;

        public ConvexPolygon(int peakCount, Vector4? color = null)
        {
            PeakCount = peakCount;
            Color = color ?? BitmapColor.White;
        }
    }

    public struct Star : IBasicShape
    {
        public Vector4 Color;
        public int PeakCount;
        public int PeakStep;

        public Star(int peakCount, int peakStep, Vector4? color = null)
        {

            PeakCount = peakCount;
            PeakStep = peakStep;
            Color = color ?? BitmapColor.White;
        }
    }

    public struct RayStar : IBasicShape
    {
        public Vector4 Color;
        public int PeakCount;

        public RayStar(int peakCount, Vector4? color = null)
        {
            
            PeakCount = peakCount;
            Color = color ?? BitmapColor.White;
        }
    }

    public struct Offset : IBasicShape
    {
        public int Value;

        public Offset(int value)
        {
            Value = value;
        }
    }

    public struct Runes : IBasicShape
    {
        public string Value;

        public Runes(int length)
        {
            Value = Program.RandomString(length);
        }
        public Runes(string runes)
        {
            Value = runes;
        }
    }
}