﻿namespace SwordAttackProto.code_asbest.Gameplay.Magic
{
    public class Layer
    {
        public IBasicShape[] Shapes;
        public Layer(params IBasicShape[] shapes)
        {
            Shapes = shapes;
        }
    }
}