﻿using System;
using OpenTK;
using SharpFont;
using SwordAttackProto.code_asbest.Common;
using SwordAttackProto.code_asbest.Graphics;
using SwordAttackProto.code_asbest.Graphics.Text;

namespace SwordAttackProto.code_asbest.Gameplay.Magic
{
    /* ====================================
     *      RIP 15 Jul - 07 Oct 2020
     *         Elder RuneCircle.CS
     *          `Save in memory`
     * ====================================
     *                 .
     *                -|-
     *                 |
     *             .-'~~~`-.
     *           .'         `.
     *           |  R  I  P  |
     *           |           |
     *           |           |
     *         \\|           |//
     * ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
     * ====================================
     */

    internal class RuneCircle
    {
        public Drawable[] Shapes { get; private set; }
        public Vector3 Position;
        public Vector3 Rotation = Vector3.Zero;

        private const int RadiusOffset = 30;
        public double StartTime = 0;
        public float CastTime = 3f;

        private Layer[] _layers;


        public RuneCircle(params Layer[] layers)
        {
            _layers = layers;
            //Shape = Drawable.Create2d(0, 0, -1, 1, 1);
            Position = Vector3.Zero;

            Rebuild();
        }

        public double GetTimeDifference()
        {
            if(StartTime == 0)
                StartTime = Helper.GetCurrentTimestamp() / 1_000d;

            return Helper.GetCurrentTimestamp() / 1_000d - StartTime;
        }
        public float GetCurrentFrame(float duration, float offset=0) => MathF.Sin(Math.Clamp((float)(GetTimeDifference()+offset) / duration, 0f, MathF.PI/2));
        public float GetCurrentFrameEndly(float duration, float time) => GetCurrentFrame(duration)-GetCurrentFrame(duration, -time);
        public void Rebuild()
        {
            var maxRadius = RadiusOffset;
            var radius = 0;
            const int height = 25;
            foreach (var layer in _layers)
            {
                foreach (var shape in layer.Shapes)
                {
                    if (shape is Offset offset)
                        maxRadius += offset.Value;
                    else if (shape is Runes)
                        maxRadius += height * 7 / 4;
                }
            }

            var center = new Vector2(maxRadius);


            var library = new Library();

            var face = new Face(library, "assets/fonts/FUTHA___.TTF");

            face.SetCharSize(
                0, /* char_width in 1/64th of points  */
                height, /* char_height in 1/64th of points */
                100, /* horizontal device resolution    */
                100 /* vertical device resolution      */
            );


            Shapes = new Drawable[_layers.Length];

            for (var i = 0; i < _layers.Length; i++)
            {
                var canvas = new BitmapCanvas();
                canvas.Expand(maxRadius * 2, maxRadius * 2);
                foreach (var shape in _layers[i].Shapes)
                {
                    switch (shape)
                    {
                        case Offset offset:
                            radius += offset.Value;
                            break;
                        case Circle circle:
                            canvas.DrawCircle(radius, center, circle.Color);
                            break;
                        case ConvexPolygon convexPolygon:
                            canvas.DrawConvexPolygon(radius, center, convexPolygon.PeakCount,
                                convexPolygon.Color);
                            break;
                        case Star star:
                            canvas.DrawStar(radius, center, star.PeakCount, star.PeakStep, star.Color);
                            break;
                        case RayStar rayStar:
                            canvas.DrawRayStar(radius, center, rayStar.PeakCount, rayStar.Color);
                            break;
                        case Runes runes:
                            var rotation = 0.0f;
                            radius += (int) (height * 0.75f);
                            
                            foreach (var rune in runes.Value)
                            {
                                rotation += 2 * MathF.PI / runes.Value.Length;

                                var x = (int) (center.X + MathF.Cos(rotation) * radius) - height / 2;
                                var y = (int) (center.Y + MathF.Sin(rotation) * radius) - height / 2;
                                var angle = MathF.PI + MathF.PI / 2 - MathF.Atan2(y - center.Y, x - center.X);

                                var glyphIndex = face.GetCharIndex(rune);


                                face.LoadGlyph(glyphIndex, LoadFlags.Default, LoadTarget.Normal);

                                var glyph = face.Glyph.GetGlyph();


                                glyph.Transform(
                                    FtHelper.CreateRotationMatrix(angle),
                                    new FTVector(0, 0));
                                glyph.ToBitmap(RenderMode.Normal, new FTVector26Dot6(0, 0), true);

                                var box = glyph.GetCBox(GlyphBBoxMode.Pixels);
                                var width = box.Right - box.Left;

                                if (width == 0)
                                    continue;

                                float vv = i * i / _layers.Length * MathF.PI/2;
                                var bitmapGlyph = glyph.ToBitmapGlyph();
                                canvas.AddMono(bitmapGlyph.Bitmap.BufferData, x, y, width, (byte)(255*(1f+MathF.Sin(vv))/2f), (byte)(255 * (1f + MathF.Cos(vv)) / 2f), (byte)(255 * (1f + MathF.Tanh(vv)) / 2f));
                            }
                            
                            radius += height;

                            break;
                    }
                }

                Shapes[i] = Drawable.Create2d(0, 0, -1, 1, 1);
                Shapes[i].Position = Position;
                Shapes[i].Scale = new Vector3(canvas.Width, canvas.Height, 1);
                Shapes[i].Texture = new Texture(canvas.ToRgba(), canvas.Width, canvas.Height);
                Shapes[i].Scale = new Vector3(maxRadius / 100f);
            }
        }

        public void Draw(DrawingData data)
        {
            data.Projection = data.Perspective;
            data.Shader = Assets.Shaders["rune_circle"];
            data.Shader.SetVector3("color", new Vector3(1, 1, 1));

            for (var i = 0; i < Shapes.Length; i++)
            {
                var shape = Shapes[i];
                
                var posMul = MathF.Exp(GetCurrentFrame(0.15f))/MathF.E;
                
                shape.Origin = new Vector3(0.5f, 0.5f, 0)*posMul;
                var mul = 1f - GetCurrentFrame(CastTime);
                
                shape.AnimationRotate = new Vector3(0.1f, 0.6f, 0.3f)*(i+1)*MathF.Exp(mul)*mul + new Vector3(0, 0, MathF.Exp(GetCurrentFrameEndly(1f/(float)CastTime, CastTime))*MathF.PI / (i + 4) * data.GlobalTime);
                shape.AnimationPosition = (1f-posMul) * new Vector3(MathF.Cos( (i)*MathF.PI/3), 0, MathF.Sin((i+(1f)*MathF.PI*2)*MathF.PI/3))*3f;
                shape.AnimationScale = new Vector3(posMul);
                
                
                data.Shader.SetVector3("color",
                    new Vector3(MathF.Exp(GetCurrentFrameEndly(1f/(float)CastTime, CastTime)*2f)));
                
                shape.Draw(data);
            }
        }
    }
}