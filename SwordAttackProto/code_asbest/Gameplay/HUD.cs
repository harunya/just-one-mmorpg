using System;
using System.Collections.Generic;
using OpenTK;
using SwordAttackProto.code_asbest.Common;
using SwordAttackProto.code_asbest.Graphics;

namespace SwordAttackProto.code_asbest.Gameplay
{
    // ReSharper disable once InconsistentNaming
    internal enum HUDType {
        PROGRESS_BAR,
        ICON
    }

    // ReSharper disable once InconsistentNaming
    public class HUDElement
    {
        protected Drawable Object {get; set;}
        protected Vector2 _pos;
        public Vector2 Position {
            get => _pos;
            set
            {
                _pos = value;
                if(Object != null){
                    Object.Position = new Vector3(value.X, value.Y, -1);
                }
                
            }
        }
        protected Vector2 _size;
        public Vector2 Size {get => GetSize(); private set => _size = value;}
        public string Name {get; private set;}

        public bool Display {get; set;} = true;
        
        public HUDElement(int x, int y, int width, int height, string name=""){
            var maxSize = Math.Max(width, height);

            var storage = new VertexStorage(
                new[]{
                    new Vertex(
                        new[]{
                            0f, height, 0f,
                            0f, 0f, 0f,
                            width, 0f, 0f,

                            0, height, 0f,
                            width, 0f, 0f,
                            width, height, 0f
                        }, 0, 3
                    ),
                    new Vertex(
                        new[]{
                            0f, 1f,
                            0f, 0f,
                            1f, 0f,

                            0f, 1f,
                            1f, 0f,
                            1f, 1f
                        }, 1, 2
                    )
                }
            );

            Object = new Drawable(storage);

            Object.Position = new Vector3(x, y, -1);
            //Object.Scale = 1f;

            Size = new Vector2(width, height);
            Position = new Vector2(x, y);

            Name = name;
        }
        
        public virtual void Draw(DrawingData data){
            if(!Display)
                return;
                
            Object.Draw(data);
        }

        public virtual Vector2 GetSize(){
            return _size;
        }
    }


    public enum LayoutDirection {
        LEFT_RIGHT = 3,
        TOP_BOTTOM = 2,
        BOTTOM_TOP = 1,
        RIGHT_LEFT = 0
    }
    
    public class HUDContainer : HUDElement
    {
        private List<HUDElement> Elements {get; set;} = new List<HUDElement>();
        private LayoutDirection Direction = LayoutDirection.TOP_BOTTOM;
        private int Padding {get; set;}

        public HUDContainer(string name, int x, int y, int width, int height, int padding, params HUDElement[] elements) : base(x, y, width, height, name)
        {
            Object = null;
            Padding = padding;

            Add(elements);
        }

        public HUDContainer(string name, LayoutDirection direction, int x, int y, int padding, params HUDElement[] elements) : this(name, x, y, 0, 0, padding, elements)
        {
            Direction = direction;
        }

        public HUDContainer(string name, LayoutDirection direction, int padding, params HUDElement[] elements) : this(name, direction, 0, 0, padding, elements)
        {}


        public void Add(params HUDElement[] elements){
            
            foreach(var element in elements)
                Elements.Add(element);
        }

        public T Get<T>(string name) where T : class
        {
            
            foreach(var element in Elements){
                if(element.Name == name)
                    return element as T;
            }

            return null;   
        }

        public override void Draw(DrawingData data){
            var elements = Elements.ToArray();

            if((int)Direction % 2 == 0)
                Array.Reverse(elements);

            float offset = 0;
            foreach (var el in elements)
            {
                var e = el;

                if(Direction == LayoutDirection.TOP_BOTTOM || Direction == LayoutDirection.BOTTOM_TOP ){
                    e.Position = new Vector2(Position.X, Position.Y + offset);
                    offset += e.Size.Y;
                }else{
                    e.Position = new Vector2(Position.X + offset, Position.Y);
                    offset += e.Size.X;
                }

                offset += Padding;


                e.Draw(data);
            }
        }

        public override Vector2 GetSize(){
            var maxSize = new Vector2();

            foreach (var e in Elements)
            {
                maxSize.X = e.GetSize().X > maxSize.X ? e.GetSize().X : maxSize.X;
                maxSize.Y = e.GetSize().Y > maxSize.Y ? e.GetSize().Y : maxSize.Y;
            }

            return maxSize;
        }
    }

    public class HUDIcon : HUDElement
    {
        public HUDIcon(string name, int x, int y, int width, int height, Texture texture) : base(x, y, width, height, name)
        {
            Object.Texture = texture;
        }
    }

    public class HUDProgress : HUDElement
    {
        public Vector4 Color {get; set;} = new Vector4(0.1f, 0.4f, 0.7f, 1f);
        public Vector4 BackgroundColor {get; set;} = new Vector4(1f);
        public Vector4 BorderColor {get; set;} = new Vector4(0.7f, 0.7f, 0.7f, 1f);
        public float BorderSize {get; set;} = 2f;
        public float Value {get; set;} = 0.7f;

        public HUDProgress(string name, int x, int y, int width, int height) : base(x, y, width, height, name)
        {}

        public HUDProgress(string name, int x, int y, int width, int height, Vector4 color) : base(x, y, width, height, name)
        {
            Color = color;
        }

        public override void Draw(DrawingData data){
            var shader = Assets.Shaders["progress"];
            shader.SetVector4("color", Color);
            shader.SetVector2("size", Size);
            shader.SetVector4("backgound_color", BackgroundColor);
            shader.SetFloat("border", BorderSize);
            shader.SetFloat("value", Value);
            shader.SetVector2("position", Position);
            shader.SetVector4("border_color", BorderColor);

            data.Projection = data.Orthogonal;
            data.Shader = shader;

            base.Draw(data);
        }
    }


    // ReSharper disable once InconsistentNaming
    public class HUD
    {
        private Dictionary<string, HUDElement> Elements {get; set;} = new Dictionary<string, HUDElement>();

        public void Add(params HUDElement[] elements){
            //var container = new HUDContainer(0,0,0,0);
            foreach (var e in elements)
            {
                Elements.Add(e.Name, e);
            }
            
        }
        public T Get<T>(string name) where T : class{
            return Elements[name] as T;
        }

        public void Draw(DrawingData data){
            data.Projection = data.Orthogonal;

            foreach (var item in Elements)
            {
                item.Value.Draw(data);
            }
        }

    }

}