using System.Collections.Generic;

namespace SwordAttackProto.code_asbest.Common
{
    public static class Assets
    {
        public static Dictionary<string, Shader> Shaders = new Dictionary<string, Shader>();
        static Assets (){
            // TODO: practice shader managing in file
            Shaders.Add("default", new Shader("assets/shaders/shader.vert", "assets/shaders/shader.frag"));
            Shaders.Add("skybox", new Shader("assets/shaders/skybox.vert", "assets/shaders/skybox.frag"));
            //Shaders.Add("test", new Shader("assets/shaders/shader.vert", "assets/shaders/test.frag"));
            Shaders.Add(
                "terrain",
                new Shader(
                    "assets/shaders/terrain.vert", 
                    "assets/shaders/terrain.frag",
                    "assets/shaders/terrain.tcs",
                    "assets/shaders/terrain.tes"
                    )
                );
            Shaders.Add("HUD", new Shader("assets/shaders/hud.vert", "assets/shaders/hud.frag"));
            Shaders.Add("canvas", new Shader("assets/shaders/canvas.vert", "assets/shaders/canvas.frag"));
            Shaders.Add("progress", new Shader("assets/shaders/hud.vert", "assets/shaders/progress.frag"));
            //Shaders.Add("entity", new Shader("assets/shaders/entity.vert", "assets/shaders/shader.frag"));
            Shaders.Add("sword", new Shader("assets/shaders/sword.vert", "assets/shaders/shader.frag"));
            Shaders.Add("sword_tracearea", new Shader("assets/shaders/sword_tracearea.vert", "assets/shaders/sword_tracearea.frag"));
            Shaders.Add("cursor", new Shader("assets/shaders/cursor.vert", "assets/shaders/cursor.frag"));
            Shaders.Add("rune_circle", new Shader("assets/shaders/runecircle.vert", "assets/shaders/runecircle.frag"));
            Shaders.Add("blur", new Shader("assets/shaders/blur.vert", "assets/shaders/blur.frag"));
            Shaders.Add("mix", new Shader("assets/shaders/blur.vert", "assets/shaders/mix.frag"));
        }
    }

}