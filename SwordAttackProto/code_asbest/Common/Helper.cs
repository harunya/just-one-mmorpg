﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using OpenTK;
using Rectangle = System.Drawing.Rectangle;

namespace SwordAttackProto.code_asbest.Common
{

    [StructLayout(LayoutKind.Sequential)]
    public class Rect
    {
        #region Variables.
        /// <summary>
        /// Left position of the rectangle.
        /// </summary>
        public int Left;
        /// <summary>
        /// Top position of the rectangle.
        /// </summary>
        public int Top;
        /// <summary>
        /// Right position of the rectangle.
        /// </summary>
        public int Right;
        /// <summary>
        /// Bottom position of the rectangle.
        /// </summary>
        public int Bottom;
        #endregion

        #region Operators.
        /// <summary>
        /// Operator to convert a RECT to Drawing.Rectangle.
        /// </summary>
        /// <param name="rect">Rectangle to convert.</param>
        /// <returns>A Drawing.Rectangle</returns>
        public static implicit operator Rectangle(Rect rect)
        {
            return Rectangle.FromLTRB(rect.Left, rect.Top, rect.Right, rect.Bottom);
        }

        /// <summary>
        /// Operator to convert Drawing.Rectangle to a RECT.
        /// </summary>
        /// <param name="rect">Rectangle to convert.</param>
        /// <returns>RECT rectangle.</returns>
        public static implicit operator Rect(Rectangle rect)
        {
            return new Rect(rect.Left, rect.Top, rect.Right, rect.Bottom);
        }
        #endregion

        #region Constructor.
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="left">Horizontal position.</param>
        /// <param name="top">Vertical position.</param>
        /// <param name="right">Right most side.</param>
        /// <param name="bottom">Bottom most side.</param>
        public Rect(int left, int top, int right, int bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }
        #endregion
    }

    [StructLayout(LayoutKind.Sequential)]
    public class Drawtextparams
    {
        public uint cbSize;
        public int iTabLength;
        public int iLeftMargin;
        public int iRightMargin;
        public uint uiLengthDrawn;
    }

    public enum TernaryRasterOperations : uint
    {
        /// <summary>dest = source</summary>
        SRCCOPY = 0x00CC0020,
        /// <summary>dest = source OR dest</summary>
        SRCPAINT = 0x00EE0086,
        /// <summary>dest = source AND dest</summary>
        SRCAND = 0x008800C6,
        /// <summary>dest = source XOR dest</summary>
        SRCINVERT = 0x00660046,
        /// <summary>dest = source AND (NOT dest)</summary>
        SRCERASE = 0x00440328,
        /// <summary>dest = (NOT source)</summary>
        NOTSRCCOPY = 0x00330008,
        /// <summary>dest = (NOT src) AND (NOT dest)</summary>
        NOTSRCERASE = 0x001100A6,
        /// <summary>dest = (source AND pattern)</summary>
        MERGECOPY = 0x00C000CA,
        /// <summary>dest = (NOT source) OR dest</summary>
        MERGEPAINT = 0x00BB0226,
        /// <summary>dest = pattern</summary>
        PATCOPY = 0x00F00021,
        /// <summary>dest = DPSnoo</summary>
        PATPAINT = 0x00FB0A09,
        /// <summary>dest = pattern XOR dest</summary>
        PATINVERT = 0x005A0049,
        /// <summary>dest = (NOT dest)</summary>
        DSTINVERT = 0x00550009,
        /// <summary>dest = BLACK</summary>
        BLACKNESS = 0x00000042,
        /// <summary>dest = WHITE</summary>
        WHITENESS = 0x00FF0062,
        /// <summary>
        /// Capture window as seen on screen.  This includes layered windows
        /// such as WPF windows with AllowsTransparency="true"
        /// </summary>
        CAPTUREBLT = 0x40000000
    }

    public enum BitmapCompressionMode : uint
    {
        BI_RGB = 0,
        BI_RLE8 = 1,
        BI_RLE4 = 2,
        BI_BITFIELDS = 3,
        BI_JPEG = 4,
        BI_PNG = 5
    }

    [StructLayout(LayoutKind.Sequential, Pack = 1)]
    public struct Rgbquad
    {
        public byte rgbBlue;
        public byte rgbGreen;
        public byte rgbRed;
        public byte rgbReserved;
    }

    [StructLayout(LayoutKind.Sequential)]
    public struct Bitmapinfoheader
    {
        public uint biSize;
        public int biWidth;
        public int biHeight;
        public ushort biPlanes;
        public ushort biBitCount;
        public BitmapCompressionMode biCompression;
        public uint biSizeImage;
        public int biXPelsPerMeter;
        public int biYPelsPerMeter;
        public uint biClrUsed;
        public uint biClrImportant;

        public void Init()
        {
            biSize = (uint)Marshal.SizeOf(this);
        }
    }

    internal enum DibColorMode : uint
    {
        DIB_RGB_COLORS = 0,
        DIB_PAL_COLORS = 1
    }

    [StructLayout(LayoutKind.Sequential)]

    public struct Ttpolygonheader

    {

        public int cb;
        public int dwType;
        [MarshalAs(UnmanagedType.Struct)] public Pointfx pfxStart;
    }

    [StructLayout(LayoutKind.Sequential)]

    public struct Ttpolycurveheader
    {

        public short wType;
        public short cpfx;
    }

    [StructLayout(LayoutKind.Sequential)]

    public struct Fixed

    {

        public short fract;
        public short value;
    }

    [StructLayout(LayoutKind.Sequential)]

    public struct Mat2

    {

        [MarshalAs(UnmanagedType.Struct)] public Fixed eM11;
        [MarshalAs(UnmanagedType.Struct)] public Fixed eM12;
        [MarshalAs(UnmanagedType.Struct)] public Fixed eM21;
        [MarshalAs(UnmanagedType.Struct)] public Fixed eM22;
    }

    [StructLayout(LayoutKind.Sequential)]

    public struct Point
    {

        public int x;
        public int y;
    }

    [StructLayout(LayoutKind.Sequential)]

    public struct Pointfx
    {

        [MarshalAs(UnmanagedType.Struct)] public Fixed x;
        [MarshalAs(UnmanagedType.Struct)] public Fixed y;
    }

    [StructLayout(LayoutKind.Sequential)]

    public struct Glyphmetrics
    {

        public int gmBlackBoxX;
        public int gmBlackBoxY;
        [MarshalAs(UnmanagedType.Struct)] public Point gmptGlyphOrigin;
        public short gmCellIncX;
        public short gmCellIncY;
    }


    internal class Helper
    {
        public static DrawingData DrawingDataDefault {
            get {
                var data = new DrawingData();

                data.Model = Matrix4.CreateTranslation(0, 0, 0);
                data.Orthogonal = Matrix4.Zero;
                data.Perspective = Matrix4.Zero;
                data.View = Matrix4.Zero;
                data.Shader = null;


                return data;
            }
        }

        public static float Linearin(float p1, float p2, float fraction) { return p1 + (p2 - p1) * fraction; }
        public static Vector2 Linearin(Vector2 src, Vector2 dist, float fraction) {
            return new Vector2(Linearin(src.X, dist.X, fraction), Linearin(src.Y, dist.Y, fraction));
        }

        [DllImport("user32.dll")]
        public static extern bool ClipCursor(Rect lpRect);

        [DllImport("user32.dll")]
        public static extern int DrawTextEx(IntPtr hdc, StringBuilder lpchText, int cchText, ref Rect lprc, uint dwDtFormat, Drawtextparams lpDtParams);

        [DllImport("gdi32.dll", EntryPoint = "CreateCompatibleDC", SetLastError = true)]
        public static extern IntPtr CreateCompatibleDC([In] IntPtr hdc);

        [DllImport("user32.dll")]
        public static extern uint GetLastError();

        [DllImport("gdi32.dll", EntryPoint = "BitBlt", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool BitBlt([In] IntPtr hdc, int nXDest, int nYDest, int nWidth, int nHeight, [In] IntPtr hdcSrc, int nXSrc, int nYSrc, TernaryRasterOperations dwRop);

        [DllImport("gdi32.dll", EntryPoint = "GetDIBits")]
        public static extern int GetDIBits([In] IntPtr hdc, [In] IntPtr hbmp, uint uStartScan, uint cScanLines, [Out] byte[] lpvBits, ref Bitmapinfoheader lpbi, DibColorMode uUsage);

        [DllImport("gdi32.dll", EntryPoint = "CreateCompatibleBitmap")]
        public static extern IntPtr CreateCompatibleBitmap([In] IntPtr hdc, int nWidth, int nHeight);

        [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
        public static extern IntPtr SelectObject([In] IntPtr hdc, [In] IntPtr hgdiobj);

        [DllImport("gdi32.dll")]
        public static extern int GetObject(IntPtr hgdiobj, int cbBuffer, IntPtr lpvObject);
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr GetDC(IntPtr hWnd);
        [DllImport("gdi32.dll")]
        public static extern uint GetGlyphOutline(IntPtr hdc, uint uChar, uint uFormat, out Glyphmetrics lpgm, uint cbBuffer, IntPtr lpvBuffer, ref Mat2 lpmat2);

        public static long GetCurrentTimestamp()
        {
            return new DateTimeOffset(DateTime.UtcNow).ToUnixTimeMilliseconds();
        }
    }
}
