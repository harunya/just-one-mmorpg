namespace SwordAttackProto.code_asbest.Common
{
    public class Vertex
    {
        public float[] Points {get; private set;}
        public int Handle {get; private set;}
        public int Size {get; private set;}
        public int Offset {get; set; }

        public Vertex(float[] points, int handle, int size){
            Points = points;
            Handle = handle;
            Size = size;
        }
    }
}