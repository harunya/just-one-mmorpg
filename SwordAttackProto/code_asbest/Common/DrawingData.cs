﻿using OpenTK;

namespace SwordAttackProto.code_asbest.Common
{
    public struct DrawingData
    {
        public Matrix4 Perspective;
        public Matrix4 Orthogonal;
        public Matrix4 Projection;
        public Matrix4 View;
        public Matrix4 ViewWithoutPosition;
        public Matrix4 Model;
        public Shader Shader;
        public float GlobalTime;
        public float DeltaTime;
        public float CameraFov;
        public float Width;
        public float Height;
        public Vector3 CameraPosition { get; set; }
        public Vector3 CameraFront { get; set; }
    }
}
