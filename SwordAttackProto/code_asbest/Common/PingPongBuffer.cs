﻿using System;
using OpenTK.Graphics.OpenGL;
using SwordAttackProto.code_asbest.Graphics;

namespace SwordAttackProto.code_asbest.Common
{
    internal class PingPongBuffer
    {
        public int[] Handles { get; private set; }
        public int[] ColorBuffers { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }
        private Drawable Shape;

        private bool Horizontal { get; set; }


        public PingPongBuffer(int width, int height)
        {
            Width = width;
            Height = height;

            Shape = Drawable.Create2d(0, 0, -1, Width, Height, DrawableUv.FlipVerticaly);

            Handles = new int[2];
            ColorBuffers = new int[2];
            GL.GenFramebuffers(2, Handles);
            GL.GenTextures(2, ColorBuffers);
            for (var i = 0; i < 2; i++)
            {
                GL.BindFramebuffer(FramebufferTarget.Framebuffer, Handles[i]);
                GL.BindTexture(TextureTarget.Texture2D, ColorBuffers[i]);

                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb16f, Width, Height, 0, PixelFormat.Rgb, PixelType.Float, IntPtr.Zero);

                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToBorder);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToBorder);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);

                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, FramebufferAttachment.ColorAttachment0, TextureTarget.Texture2D, ColorBuffers[i], 0);
            }


            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }

        public static void Clear()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }
        public void Use(int i = 0)
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, Handles[i]);
        }

        public void Apply(DrawingData data, int initalBufferHandle, int count = 10)
        {
            var firstIteration = true;
            Horizontal = true;

            for (var i = 0; i < count * 2; i++)
            {
                data.Shader.Use();

                GL.BindFramebuffer(FramebufferTarget.Framebuffer, Handles[Horizontal ? 1 : 0]);
                data.Shader.SetInt("horizontal", Horizontal ? 1 : 0);

                var handle = firstIteration ? initalBufferHandle : ColorBuffers[!Horizontal ? 1 : 0];

                Shape.Texture = new Texture(handle);

                data.Projection = data.Orthogonal;
                Shape.Draw(data);


                Horizontal = !Horizontal;
                if (firstIteration)
                    firstIteration = false;
            }


            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }

        public int GetResultHandle()
        {
            return ColorBuffers[!Horizontal ? 1 : 0];
        }

        public Texture GetResultTexture()
        {
            return new Texture(GetResultHandle());
        }

    }
}
