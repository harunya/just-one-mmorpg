﻿using System;
using System.Collections.Generic;
using OpenTK;
using SwordAttackProto.code_asbest.Gameplay.Magic;

namespace SwordAttackProto.code_asbest.Common
{
    public struct BitmapColor
    {
        public static Vector4 Black { get; private set; } = new Vector4(0, 0, 0, 255);
        public static Vector4 White { get; private set; } = new Vector4(255);
    }

    internal class BitmapCanvas
    {
        public const int DeathRange = 4;

        public int CountOfChannels { get; private set; } = 4;
        public byte[] Value { get; set; } = new byte[0];
        public int Width { get; set; }

        public int Height
        {
            get { return Width == 0 ? 0 : Value.Length / (Width * CountOfChannels); }
        }

        public void ExpandIfNeed(int x, int y, int width, int height)
        {
            #region Expand canvas

            if (x + width >= Width || y + height >= Height)
            {
                Expand(Math.Clamp(x + width - Width, 0, int.MaxValue),
                    Math.Clamp(y + height - Height, 0, int.MaxValue));
            }

            #endregion
        }

        public void AddMono(byte[] arr, int x, int y, int width, byte r = 255, byte g = 255, byte b = 255, byte a = 255)
        {
            //var newWidth = (width * CountOfChannels);
            var height = arr.Length / width;

            ExpandIfNeed(x, y, width, height);


            #region Transfer bitmap to canvas

            for (var ix = x; ix < x + width; ix++)
            {
                for (var iy = y; iy < y + height; iy++)
                {
                    var srcValue = (byte) (arr[GetIndex(ix - x, iy - y, width)] / 255);

                    AddPixel(ix, iy, (byte) (srcValue * r), (byte) (srcValue * g), (byte) (srcValue * b),
                        (byte) (srcValue * a));
                }
            }

            #endregion
        }

        public void DrawCircle(int r, Vector2 center, Vector4 color, int width = 5)
        {
            const float e = 100f;
            for (var x = (int) (center.X - r - width); x < center.X + r + width; x++)
            for (var y = (int) (center.Y - r - width); y < center.Y + r + width; y++)
            {
                var dist = MathF.Abs(MathF.Pow(x - center.X, 2) + MathF.Pow(y - center.Y, 2) - r * r) / e;
                if (dist > width) continue;
                var strength = MathF.Pow(1f - dist / width, 2);
                AddPixelWithStrength(x, y, strength, color);
            }
        }

        public static float DistanceBetweenPointAndSegment(Vector2 e, Vector2 a, Vector2 b)
        {
            var ab = b - a;
            var ae = e - a;
            var be = e - b;

            if (Vector2.Dot(ab, be) > 0)
                return Vector2.Distance(e, b);
            if (Vector2.Dot(ab, ae) < 0)
                return Vector2.Distance(e, a);
            return MathF.Abs(ab.X * ae.Y - ab.Y * ae.X) / MathF.Sqrt(ab.X * ab.X + ab.Y * ab.Y);
        }

        public void DrawLine(Vector4 line, Vector4 color, int width = 5)
        {
            DrawLine((int) line.X, (int) line.Y, (int) line.Z, (int) line.W, color, width);
        }

        public void DrawLine(Vector2 point1, Vector2 point2, Vector4 color, int width = 5)
        {
            DrawLine((int) point1.X, (int) point1.Y, (int) point2.X, (int) point2.Y, color, width);
        }

        public void DrawLine(int x0, int y0, int x1, int y1, Vector4 color, int width)
        {
            if (MathF.Abs(x1 - x0) > MathF.Abs(y1 - y0))
            {
                var k = (float) -(y1 - y0) / (x0 - x1);
                var b = (float) -(x1 * y0 - x0 * y1) / (x0 - x1);
                for (var x = (int) MathF.Min(x0, x1) - width; x < MathF.Max(x0, x1) + width; x++)
                for (var i = -width; i < width; i++)
                {
                    var y = (int) (k * x + b + i);
                    var dist = DistanceBetweenPointAndSegment(
                        new Vector2(x, y),
                        new Vector2(x0, y0),
                        new Vector2(x1, y1));
                    if (dist > width) continue;
                    var strength = MathF.Pow(1f - dist / width, 2);
                    AddPixelWithStrength(x, y, strength, color);
                }
            }
            else
            {
                var k = (float) -(x1 - x0) / (y0 - y1);
                var b = (float) -(y1 * x0 - y0 * x1) / (y0 - y1);
                for (var y = (int) MathF.Min(y0, y1) - width; y < MathF.Max(y0, y1) + width; y++)
                for (var i = -width; i < width; i++)
                {
                    var x = (int) (k * y + b + i);
                    var dist = DistanceBetweenPointAndSegment(
                        new Vector2(y, x),
                        new Vector2(y0, x0),
                        new Vector2(y1, x1));
                    if (dist > width) continue;
                    var strength = MathF.Pow(1f - dist / width, 2);
                    AddPixelWithStrength(x, y, strength, color);
                }
            }
        }

        public void DrawLines(IEnumerable<Vector4> lines, Vector4? color = null)
        {
            if (color == null)
                color = BitmapColor.White;
            foreach (var line in lines)
                DrawLine(line, color.Value);
        }

        public void DrawLinesStrip(Vector2[] points, Vector4? color = null, bool isLoop = true)
        {
            if (color == null)
                color = BitmapColor.White;


            if (isLoop)
                for (var i = 0; i < points.Length; i++)
                    DrawLine(points[i], points[(i + 1) % points.Length], color.Value);
            else
                for (var i = 0; i < points.Length - 1; i++)
                    DrawLine(points[i], points[i + 1], color.Value);
        }

        public void DrawStar(float r, Vector2 center, int peakCount, int peakStep, Vector4? color = null)
        {
            color ??= BitmapColor.White;
            DrawLinesStrip(Shape.LsStar(r, center, peakCount, peakStep), color.Value);
        }

        public void DrawRayStar(float r, Vector2 center, int peakCount, Vector4? color = null)
        {
            color ??= BitmapColor.White;
            DrawLines(Shape.LRayStar(r, center, peakCount), color.Value);
        }

        public void DrawConvexPolygon(float r, Vector2 center, int peakCount, Vector4? color = null)
        {
            color ??= BitmapColor.White;
            DrawLinesStrip(Shape.LsConvexPolygon(r, center, peakCount), color.Value);
        }

        public byte[] GetPixel(int x, int y)
        {
            if (x / Width > 1)
                throw new Exception("Oversize get");


            var result = new byte[CountOfChannels];
            Array.Copy(Value, y * Width + x, result, 0, CountOfChannels);

            return result;
        }

        public void SetPixel(int x, int y, params byte[] value)
        {
            if (x / Width > 1)
                throw new Exception("Oversize set");
            if (value.Length < CountOfChannels)
                throw new Exception("Value counts invalid");

            for (var i = 0; i < CountOfChannels; i++)
            {
                Value[y * (Width * CountOfChannels) + x * CountOfChannels + i] = value[i];
            }
        }


        private void AddPixelWithStrength(int x, int y, float strength, Vector4 color)
        {
            AddPixel(x, y,
                (byte) (strength * color.X),
                (byte) (strength * color.Y),
                (byte) (strength * color.Z),
                (byte) (strength * color.W));
        }

        public void AddPixel(int x, int y, params byte[] value)
        {
            if (x / Width > 1)
                throw new Exception("Oversize set");
            if (value.Length < CountOfChannels)
                throw new Exception("Value counts invalid");

            for (var i = 0; i < CountOfChannels; i++)
            {
                if (value[i] > Value[y * (Width * CountOfChannels) + x * CountOfChannels + i])
                    Value[y * (Width * CountOfChannels) + x * CountOfChannels + i] = value[i];
            }
        }

        public void Expand(int x, int y)
        {
            var temp = new List<byte>();

            for (var row = 0; row < Height + y; row++)
            {
                for (var i = row * Width * CountOfChannels; i < (row + 1) * Width * CountOfChannels; i++)
                {
                    temp.Add(i >= Value.Length ? (byte) 0 : Value[i]);
                }

                for (var j = 0; j < x * CountOfChannels; j++)
                {
                    temp.Add(0);
                }
            }


            Width += x;

            Value = temp.ToArray();
        }

        public byte[] ToRgba()
        {
            return Value;
        }

        public static int GetIndex(int x, int y, int width)
        {
            return y * width + x % width;
        }
    }
}