﻿using System;
using OpenTK.Graphics.OpenGL;
using SwordAttackProto.code_asbest.Common;

namespace SwordAttackProto.code_asbest.Graphics
{
    internal class FrameBuffer
    {
        public int Handle { get; private set; }
        public int[] ColorBuffers { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        public PingPongBuffer TransformationBuffer { get; private set; }


        public FrameBuffer(int width, int height)
        {
            var colorBuffersCount = 2;


            Handle = GL.GenFramebuffer();
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, Handle);

            ColorBuffers = new int[colorBuffersCount];

            GL.GenTextures(2, ColorBuffers);

            for (uint i = 0; i < colorBuffersCount; i++)
            {
                GL.BindTexture(TextureTarget.Texture2D, ColorBuffers[i]);
                GL.TexImage2D(TextureTarget.Texture2D, 0, PixelInternalFormat.Rgb16f, width, height, 0, PixelFormat.Rgb, PixelType.Float, IntPtr.Zero);

                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapS, (int)TextureWrapMode.ClampToBorder);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureWrapT, (int)TextureWrapMode.ClampToBorder);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMinFilter, (int)TextureMinFilter.Nearest);
                GL.TexParameter(TextureTarget.Texture2D, TextureParameterName.TextureMagFilter, (int)TextureMagFilter.Nearest);

                
                GL.FramebufferTexture2D(FramebufferTarget.Framebuffer, (FramebufferAttachment)((int)FramebufferAttachment.ColorAttachment0 + i), TextureTarget.Texture2D, ColorBuffers[i], 0);
            }

            DrawBuffersEnum[] attachments = { DrawBuffersEnum.ColorAttachment0, DrawBuffersEnum.ColorAttachment1 };
            GL.DrawBuffers(2, attachments);


            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);


            TransformationBuffer = new PingPongBuffer(width, height);
        }

        public Texture Apply(DrawingData data, int power = 10)
        {
            TransformationBuffer.Apply(data, ColorBuffers[1], power);

            return TransformationBuffer.GetResultTexture();
        }

        public Texture GetTexture()
        {
            return new Texture(ColorBuffers[0]);
        }

        public void Use()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, Handle);
        }


        public static void Unbind()
        {
            GL.BindFramebuffer(FramebufferTarget.Framebuffer, 0);
        }

        public static void Clear()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);
        }
    }
}
