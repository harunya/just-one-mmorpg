namespace SwordAttackProto.code_asbest.Common
{
    public struct VertexAttribute
    {
        public VertexAttribute(int bindingId, int size, int stride, int offset)
        {
            BindingId = bindingId;
            Size = size;
            Stride = stride;
            Offset = offset;
        }

        public int BindingId;
        public int Size;
        public int Stride;
        public int Offset;
    }
    
    public class VertexStorage
    {
        public Vertex[] Vertices {get; private set;}
        public int PointsCount {get; private set;}
        public float[] Points {get; private set;}
        public int Sizes {get; private set;}


        public VertexStorage(Vertex[] vertices){
            Vertices = vertices;

            Points = GetArray();
            PointsCount = Points.Length;
        }

        public void Clear()
        {
            Points = null;
        }
        public float[] GetArray(){
            Sizes = 0;
            var points = 0;

            foreach (var vertex in Vertices)
            {
                vertex.Offset = Sizes;
                Sizes += vertex.Size;
                points += vertex.Points.Length;
            }

            var arr = new float[points];
            for(int i=0, k=0;i < arr.Length; k++){
                foreach (var vertex in Vertices)
                {
                    for(var j=0; j < vertex.Size; j++){
                        arr[i++] = vertex.Points[k*vertex.Size+j];
                    }
                }
            }
            

            return arr;
        }
    }
}