﻿using System.Collections.Generic;
using OpenTK;
using OpenTK.Graphics.OpenGL;
using SwordAttackProto.code_asbest.Common;

namespace SwordAttackProto.code_asbest.Graphics
{
    internal abstract class DrawableObject
    {
        public float[] Points { get; protected set; }
        public Drawable Object { get; protected set; }

    }

    internal class Line : DrawableObject
    {
        public Line(float x, float y, float x2, float y2)
        {
            var points = new VertexStorage(
                new[] {
                    new Vertex(new[] { x, y, x2, y2 }, 0, 2)
                }
            );

            Object = new Drawable(points, PrimitiveType.Lines);
        }
    }

    internal class Canvas
    {
        private List<DrawableObject> _objects = new List<DrawableObject>();

        public Vector3 Scale { get; set; } = new Vector3(1);
        public Vector2   Size { get; set; } = new Vector2(1);

        public void Update()
        {
            //Fill(new VertexStorage(Objects.Select(e => new Vertex(e.Points, 0, 2)).ToArray()));
        }

        public void Clear()
        {
            _objects.Clear();
        }

        public void Add(DrawableObject obj)
        {
            _objects.Add(obj);
        }

        public void Draw(DrawingData data)
        {
            _objects.ForEach(e => {
                e.Object.Scale = Scale;
                e.Object.Draw(data);
            });
        }

    }
}
