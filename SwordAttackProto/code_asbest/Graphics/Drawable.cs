using OpenTK;
using OpenTK.Graphics.OpenGL;
using SwordAttackProto.code_asbest.Common;

namespace SwordAttackProto.code_asbest.Graphics
{
    public static class DrawableUv
    {
        public static float[] Default = {
            0f, 1f,
            0f, 0f,
            1f, 0f,

            0f, 1f,
            1f, 0f,
            1f, 1f

        };

        public static float[] FlipVerticaly = {
            0f, 0f,
            0f, 1f,
            1f, 1f,

            0f, 0f,
            1f, 1f,
            1f, 0f

        };

        public static float[] default3d =
        {
            0f, 1f,
            0f, 0f,
            1f, 0f,
            0f, 1f,
            1f, 0f,
            1f, 1f,

            0f, 1f,
            0f, 0f,
            1f, 0f,
            0f, 1f,
            1f, 0f,
            1f, 1f,

            0f, 1f,
            0f, 0f,
            1f, 0f,
            0f, 1f,
            1f, 0f,
            1f, 1f,

            0f, 1f,
            0f, 0f,
            1f, 0f,
            0f, 1f,
            1f, 0f,
            1f, 1f,

            0f, 1f,
            0f, 0f,
            1f, 0f,
            0f, 1f,
            1f, 0f,
            1f, 1f,

            0f, 1f,
            0f, 0f,
            1f, 0f,
            0f, 1f,
            1f, 0f,
            1f, 1f,

        };

        public static float[] skybox =
        {
            1 / 4f, 1 / 3f,
            1 / 4f, 2 / 3f,
            2 / 4f, 2 / 3f,
            1 / 4f, 1 / 3f,
            2 / 4f, 2 / 3f,
            2 / 4f, 1 / 3f,

            4 / 4f, 1 / 3f,
            4 / 4f, 2 / 3f,
            3 / 4f, 2 / 3f,
            4 / 4f, 1 / 3f,
            3 / 4f, 2 / 3f,
            3 / 4f, 1 / 3f,

            0 / 4f, 1 / 3f,
            0 / 4f, 2 / 3f,
            1 / 4f, 2 / 3f,
            0 / 4f, 1 / 3f,
            1 / 4f, 2 / 3f,
            1 / 4f, 1 / 3f,
            
            3 / 4f, 1 / 3f,
            3 / 4f, 2 / 3f,
            2 / 4f, 2 / 3f,
            3 / 4f, 1 / 3f,
            2 / 4f, 2 / 3f,
            2 / 4f, 1 / 3f,

            2 / 4f, 2 / 3f,
            2 / 4f, 3 / 3f,
            1 / 4f, 3 / 3f,
            2 / 4f, 2 / 3f,
            1 / 4f, 3 / 3f,
            1 / 4f, 2 / 3f,

            1 / 4f, 1 / 3f,
            1 / 4f, 0 / 3f,
            2 / 4f, 0 / 3f,
            1 / 4f, 1 / 3f,
            2 / 4f, 0 / 3f,
            2 / 4f, 1 / 3f,
        };
    }

    public class Drawable
    {
        private readonly int _vbo;
        private readonly int _vao;
        private int _verticesCount;

        public Vector3 Position {get; set;} = Vector3.Zero;
        public Vector3 Rotation {get; set;} = Vector3.Zero;
        public Vector3 AnimationRotate { get; set;} = Vector3.Zero;
        public Vector3 AnimationPosition { get; set;} = Vector3.Zero;
        public Vector3 AnimationScale { get; set;} = Vector3.One;
        public Vector3 Scale {get; set;} = new Vector3(1);
        public Vector3 Origin {get; set;} = new Vector3(0,0,0);

        public Texture Texture { get; set; } = Texture.Empty;

        public PrimitiveType DrawType { get; set; } = PrimitiveType.Triangles;


        public Drawable(VertexStorage points, PrimitiveType drawType = PrimitiveType.Triangles)
        {
            Texture  = Texture.Empty;

            DrawType = drawType;

            _vbo = GL.GenBuffer();
            _vao = GL.GenVertexArray();


            Fill(points);
        }
        public Drawable()
        {
            Texture  = Texture.Empty;

            DrawType = PrimitiveType.Triangles;

            _vbo = GL.GenBuffer();
            _vao = GL.GenVertexArray();
        }

        public void Fill(VertexStorage storage){
            // 1. bind Vertex Array Object
            GL.BindVertexArray(_vao);
            
            // 2. copy our vertices array in a buffer for OpenGL to use
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, storage.PointsCount * sizeof(float), storage.Points, BufferUsageHint.StaticDraw);
            
            // 3. then set our vertex attributes pointers
            foreach (var vertex in storage.Vertices)
            {
                GL.VertexAttribPointer(
                    vertex.Handle,
                    vertex.Size,
                    VertexAttribPointerType.Float,
                    false,
                    storage.Sizes * sizeof(float),
                    vertex.Offset * sizeof(float)
                );
                
                GL.EnableVertexAttribArray(vertex.Handle);
            }
            
            // GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 5 * sizeof(float), 0);
            // GL.EnableVertexAttribArray(0);
            // GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, 5 * sizeof(float), 3 * sizeof(float));
            // GL.EnableVertexAttribArray(1);


            _verticesCount = storage.PointsCount/storage.Sizes;
        }
        
        public void Fill(float[] points, VertexAttribute[] attribs){
            // 1. bind Vertex Array Object
            GL.BindVertexArray(_vao);
            
            // 2. copy our vertices array in a buffer for OpenGL to use
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, points.Length * sizeof(float), points, BufferUsageHint.StaticDraw);
            
            // 3. then set our vertex attributes pointers
            int Sizes = 0;
            foreach (var vertex in attribs)
            {
                Sizes += vertex.Size;
                
                GL.VertexAttribPointer(
                    vertex.BindingId,
                    vertex.Size,
                    VertexAttribPointerType.Float,
                    false,
                    vertex.Stride * sizeof(float),
                    vertex.Offset * sizeof(float)
                );
                
                GL.EnableVertexAttribArray(vertex.BindingId);
            }
            
            // GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 5 * sizeof(float), 0);
            // GL.EnableVertexAttribArray(0);
            // GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, false, 5 * sizeof(float), 3 * sizeof(float));
            // GL.EnableVertexAttribArray(1);


            _verticesCount = points.Length/Sizes;
        }

        public static void SetCapabilitiesForDrawCall(DrawingData data, Texture texture, Vector3 Position, Vector3 Origin, Vector3 Rotation, Vector3 Scale)
        {
            data.Shader.Use();
            texture.Use();


            var origin = Matrix4.CreateTranslation(- new Vector3(Origin.X*Scale.X, Origin.Y * Scale.Y, Origin.Z * Scale.Z));
            var position = Matrix4.CreateTranslation(Position);

            var quater = new Quaternion(Rotation);
            var resultQuater = quater;

            Matrix4 rotation;
            Matrix4.CreateFromQuaternion(ref resultQuater, out rotation);

            Matrix4 scale;
            Matrix4.CreateScale(Scale.X, Scale.Y, Scale.Z, out scale);

            var model = scale * origin * rotation * position;
            
            data.Shader.SetMatrix4("projection", data.Projection);
            data.Shader.SetMatrix4("view", data.View);
            data.Shader.SetMatrix4("model", model);
            data.Shader.SetMatrix4("gVP", data.Projection * data.View);
            data.Shader.SetMatrix4("viewWithoutPosition", data.ViewWithoutPosition);

            //Костыль(
            //data.Shader.SetVector3("shape_position", Position);
            data.Shader.SetVector3("camera_position", data.CameraPosition);
            //data.Shader.SetVector3("shape_size", Scale);

            data.Shader.SetFloat("global_time", data.GlobalTime);
        }

        virtual public void Draw(DrawingData data){
            data.Shader.Use();
            Texture.Use();


            var origin = Matrix4.CreateTranslation(- new Vector3(Origin.X*Scale.X, Origin.Y * Scale.Y, Origin.Z * Scale.Z));
            var position = Matrix4.CreateTranslation(Position + AnimationPosition);

            var quater = new Quaternion(Rotation);
            var quaterAnimRotate = new Quaternion(AnimationRotate);
            var resultQuater = quater * quaterAnimRotate;

            Matrix4 rotation;
            Matrix4.CreateFromQuaternion(ref resultQuater, out rotation);

            Matrix4 scale;
            Matrix4.CreateScale(Scale.X * AnimationScale.X, Scale.Y * AnimationScale.Y, Scale.Z * AnimationScale.Z, out scale);

            var model = scale * origin * rotation * position;
            
            data.Shader.SetMatrix4("projection", data.Projection);
            data.Shader.SetMatrix4("view", data.View);
            data.Shader.SetMatrix4("model", model);
            data.Shader.SetMatrix4("gVP", data.Projection * data.View);
            data.Shader.SetMatrix4("viewWithoutPosition", data.ViewWithoutPosition);

            //Костыль(
            data.Shader.SetVector3("shape_position", Position);
            data.Shader.SetVector3("camera_position", data.CameraPosition);
            data.Shader.SetVector3("shape_size", Scale);

            data.Shader.SetFloat("global_time", data.GlobalTime);

            GL.BindVertexArray(_vao);
                GL.DrawArrays(DrawType, 0, _verticesCount);
            GL.BindVertexArray(0);
        }

        public static Drawable Create(float x, float y, float z, float w, float h)
        {
            var storage = new VertexStorage(
                new[]{
                    new Vertex(
                        new[]{
                            -1f/2f, 1f/2f, 0,
                            -1f/2f, -1f/2f, 0,
                            1f/2f, -1f/2f, 0,

                            -1f/2f, 1f/2f, 0,
                            1f/2f, -1f/2f, 0,
                            1f/2f, 1f/2f, 0

                        }, 0, 3
                    ),
                    new Vertex(
                        new[]{
                            0f, 1f,
                            0f, 0f,
                            1f, 0f,

                            0f, 1f,
                            1f, 0f,
                            1f, 1f

                        }, 1, 2
                    ),
                    new Vertex(
                        new[]{
                            //Forward
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f

                        }, 2, 3)
                }
            );

            var n = new Drawable(storage)
            {
                Position = new Vector3(x, y, z),
                Scale = new Vector3(w, h, 1)
            };

            return n;

        }

        public static Drawable Create2d(float x, float y, float z, float w, float h, float[] uv = null)
        {
            if (uv == null)
                uv = DrawableUv.Default;


            var storage = new VertexStorage(
                new[]{
                    new Vertex(
                        new[]{
                            0, 1f, 0,
                            0, 0, 0,
                            1f, 0, 0,

                            0, 1f, 0,
                            1f, 0, 0,
                            1f, 1f, 0

                        }, 0, 3
                    ),
                    new Vertex(
                        uv, 1, 2
                    ),
                    new Vertex(
                        new[]{
                            //Forward
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f

                        }, 2, 3)
                }
            );

            var n = new Drawable(storage)
            {
                Position = new Vector3(x, y, z),
                Scale = new Vector3(w, h, 1)
            };

            return n;

        }

        public static Drawable Create3d(float x, float y, float z, float w, float h, float l,
            float[] UV = null)
        {
            UV ??= DrawableUv.default3d;
            
            var storage = new VertexStorage(
                new[]{
                    new Vertex(
                        new float[]{
                            -1, 1, 1,
                            -1, -1, 1,
                            1, -1, 1,

                            -1, 1, 1,
                            1, -1, 1,
                            1, 1, 1,


                            -1, 1, -1,
                            -1, -1, -1,
                            1, -1, -1,

                            -1, 1, -1,
                            1, -1, -1,
                            1, 1, -1,
                            
                            // Left and right
                            
                            -1, 1, -1,
                            -1, -1, -1,
                            -1, -1, 1,

                            -1, 1, -1,
                            -1, -1, 1,
                            -1, 1, 1,


                            1, 1, -1,
                            1, -1, -1,
                            1, -1, 1,

                            1, 1, -1,
                            1, -1, 1,
                            1, 1, 1,
                            
                            // Bot and top
                            1, -1, -1,
                            -1, -1, -1,
                            -1, -1, 1,

                            1, -1, -1,
                            -1, -1, 1,
                            1, -1, 1,
                            
                            -1, 1, 1,
                            -1, 1, -1,
                            1, 1, -1,

                            -1, 1, 1,
                            1, 1, -1,
                            1, 1, 1,

                        }, 0, 3
                    ),
                    new Vertex(
                        UV, 1, 2
                    ),
                    new Vertex(
                        new[]{
                            //Forward
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,
                            0f, 0f, 1f,

                            
                            //BackWard
                            0f, 0f, -1f,
                            0f, 0f, -1f,
                            0f, 0f, -1f,
                            0f, 0f, -1f,
                            0f, 0f, -1f,
                            0f, 0f, -1f,
                            
                            //Left
                            -1f, 0f, 0f,
                            -1f, 0f, 0f,
                            -1f, 0f, 0f,
                            -1f, 0f, 0f,
                            -1f, 0f, 0f,
                            -1f, 0f, 0f,
                            
                            
                            //Right
                            1f, 0f, 0f,
                            1f, 0f, 0f,
                            1f, 0f, 0f,
                            1f, 0f, 0f,
                            1f, 0f, 0f,
                            1f, 0f, 0f,
                            
                            //Bottom
                            0f, -1f, 0f,
                            0f, -1f, 0f,
                            0f, -1f, 0f,
                            0f, -1f, 0f,
                            0f, -1f, 0f,
                            0f, -1f, 0f,
                            
                            //Top
                            0f, 1f, 0f,
                            0f, 1f, 0f,
                            0f, 1f, 0f,
                            0f, 1f, 0f,
                            0f, 1f, 0f,
                            0f, 1f, 0f,
                            

                        }, 2, 3)
                }
            );

            var n = new Drawable(storage)
            {
                Position = new Vector3(x, y, z),
                Scale = new Vector3(w, h, l)
            };

            return n;

        }
    }

}