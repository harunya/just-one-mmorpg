﻿using System;
using OpenTK;
using SharpFont;

namespace SwordAttackProto.code_asbest.Graphics.Text
{
    public static class FtHelper
    {
        public static FTMatrix ToFt(this Matrix2 mat) => 
            new FTMatrix((int)(mat.M11 * 0x10000L), (int)(mat.M12 * 0x10000L), (int)(mat.M21 * 0x10000L), (int)(mat.M22 * 0x10000L));

        public static FTMatrix CreateRotationMatrix(float angle)
        {
            return new FTMatrix
            {
                XX = new Fixed16Dot16(MathF.Cos(angle)),
                XY = new Fixed16Dot16(-MathF.Sin(angle)),
                YX = new Fixed16Dot16(MathF.Sin(angle)),
                YY = new Fixed16Dot16(MathF.Cos(angle))
            };
        }
    }
}
