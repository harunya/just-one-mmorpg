﻿using System;
using OpenTK;
using SharpFont;
using SwordAttackProto.code_asbest.Common;

namespace SwordAttackProto.code_asbest.Graphics.Text
{
    internal class Text
    {

        private int _Height = 25;
        private int _PaddingY;
        private float _Rotation;
        private string _Value;
        private Vector2 _Position = Vector2.Zero;
        private Vector2 _BoxSize = Vector2.Zero;
        private Vector4 _BorderOffset = Vector4.Zero;

        public int Height
        {
            get { return _Height; }
            set { _Height = value; Rebuild(); }
        }
        public Vector2 Position
        {
            get { return _Position; }
            set { _Position = value; _shape.Position = new Vector3(value.X, value.Y, -1); }
        }
        public Vector2 BoxSize
        {
            get { return _BoxSize; }
            set { _BoxSize = value; Rebuild(); }
        }
        public Vector4 BorderOffset
        {
            get { return _BorderOffset; }
            set { _BorderOffset = value; Rebuild(); }
        }
        public int PaddingY
        {
            get { return _PaddingY; }
            set { _PaddingY = value; Rebuild(); }
        }
        public float Rotation
        {
            get { return _Rotation; }
            set { _Rotation = value; Rebuild(); }
        }
        public string Value
        {
            get { return _Value; }
            set { _Value = value; Rebuild(); }
        }


        private Drawable _shape;
        

        public Text(string value, int size, int x = 0, int y = 0, int boxWidth = 0, int boxHeight = 0)
        {
            _Value = value;

            _Height = size;

            _Position.X = x;
            _Position.Y = y;

            _BoxSize.X = boxWidth;
            _BoxSize.Y = boxHeight;


            Rebuild();
        }

        private void Rebuild()
        {
            var library = new Library();

            var face = new Face(library, "assets/fonts/arial.ttf");

            face.SetCharSize(
                0,          /* char_width in 1/64th of points  */
                Height,    /* char_height in 1/64th of points */
                60,        /* horizontal device resolution    */
                60         /* vertical device resolution      */
            );


            var mat = Matrix2.CreateRotation(_Rotation);


            var angle = MathF.PI*MathF.Sin((float)Program.GlobalTime);

            var fmat = new FTMatrix
            {
                XX = new Fixed16Dot16(MathF.Cos(angle)),
                XY = new Fixed16Dot16(-MathF.Sin(angle)),
                YX = new Fixed16Dot16(MathF.Sin(angle)),
                YY = new Fixed16Dot16(MathF.Cos(angle))
            };


            var canvas = new BitmapCanvas();

            int penX = 0, penY = Height;

            foreach (var c in Value)
            {
                var glyphIndex = face.GetCharIndex(c);

                face.LoadGlyph(glyphIndex, LoadFlags.Default, LoadTarget.Normal);

                var glyph = face.Glyph.GetGlyph();

                glyph.Transform(FtHelper.CreateRotationMatrix(_Rotation), new FTVector(0, 0));
                glyph.ToBitmap(RenderMode.Normal, new FTVector26Dot6(0, 0), true);

                var box = glyph.GetCBox(GlyphBBoxMode.Pixels);

                var width = box.Right - box.Left;
                var height = box.Bottom - box.Top;

                var metrics = face.Glyph.Metrics;



                if (c == ' ')
                    penX += metrics.HorizontalAdvance.ToInt32();


                if (c == ' ' && BoxSize.X != 0 && penX + width >= BoxSize.X)
                {
                    penX = 0;
                    penY += Height + PaddingY;
                }

                if (width == 0)
                    continue;

                var bitmapGlyph = glyph.ToBitmapGlyph();

                canvas.AddMono(bitmapGlyph.Bitmap.BufferData, penX + metrics.HorizontalBearingX.ToInt32(), penY - metrics.HorizontalBearingY.ToInt32(), width);


                penX += metrics.HorizontalAdvance.ToInt32();
            }


            var image = new Texture(canvas.ToRgba(), canvas.Width, canvas.Height);

            _shape = Drawable.Create2d(Position.X, Position.Y, -1, canvas.Width, canvas.Height);
            _shape.Texture = image;
        }

        public void Draw(DrawingData data)
        {
            data.Projection = data.Orthogonal;
            _shape.Draw(data);
        }
    }
}
