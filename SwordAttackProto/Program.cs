﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Runtime.InteropServices;
using ImGuiNET;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SwordAttackProto.code_asbest.Common;
using SwordAttackProto.code_asbest.Gameplay;
using SwordAttackProto.code_asbest.Gameplay.Magic;
using SwordAttackProto.code_asbest.Graphics;
using SwordAttackProto.code_asbest.Graphics.Text;
using SwordAttackProto.gameplay;
using SwordAttackProto.Graphics;
using GameWindow = OpenTK.GameWindow;
using Image = SixLabors.ImageSharp.Image;
using Vector2 = System.Numerics.Vector2;

namespace SwordAttackProto
{
    public class Program : GameWindow
    {
        public static void Main()
        {
            using var game = new Program(1280, 720, "Prototype Sword Slicer");
            game.Run();
        }

        public static double GlobalTime { get; private set; }

        public static Vector3 CameraPos { get; private set; } = new Vector3(-8, 8, -8);

        public static int Fps { get; private set; }
        public static float LastFpsCheck { get; private set; }
        public static float FramesCount { get; private set; }

        public static bool GameFocused = true;
        public static bool GameCursorVisible { get; set; } = false;

        private readonly Camera _camera = new Camera(Vector3.Zero);
        private SwordPath _swordTrace;
        private Drawable _cursor;
        private Drawable _skybox;
        private Text _textShape;
        private List<RuneCircle> _testRune = new List<RuneCircle>();
        private List<RuneCircle> _testRunes = new List<RuneCircle>();
        private DrawingData _data = Helper.DrawingDataDefault;

        private FrameBuffer _frameBuffer;
        private Drawable _frameShape;
        private Map _map;

        private bool AutoWalkState = false;

        private static readonly Random Random = new Random();

        public ImguiRenderer ImguiContext;
        public bool DarkTheme = false;
        public string ConsoleLog = "";
        public float[] FpsValues = new float[60];
        
        //private readonly Dictionary<IntPtr, ResourceSetInfo> _viewsById = new Dictionary<IntPtr, ResourceSetInfo>();
        Texture Atlas;
        
        private Program(int width, int height, string title) : base(width, height, GraphicsMode.Default, title,
            GameWindowFlags.Default, DisplayDevice.Default, 3, 3, GraphicsContextFlags.Default)
        {
        }

        public static string RandomString(int length)
        {
            const string chars = "futarkgwhniepsmtblod";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        protected override void OnLoad(EventArgs e)
        {

            GL.ClearColor(0.2f, 0f, 0.1f, 0f);
            GL.Enable(EnableCap.DepthTest); 
            
            //GL.DepthFunc(DepthFunction.Lequal);
            GL.Enable(EnableCap.Blend);
            //GL.Enable(EnableCap.CullFace);
            //GL.CullFace(CullFaceMode.Front);
            //GL.FrontFace(FrontFaceDirection.Ccw);
            GL.BlendFunc((BlendingFactor) BlendingFactorSrc.One, (BlendingFactor) BlendingFactorDest.OneMinusSrcAlpha);

            UpdateMatrix();
            UpdateBuffers();

            _skybox = Drawable.Create3d(0, 0, 0, 1, 1, 1, DrawableUv.skybox);
            _skybox.Texture = new Texture("assets/textures/skybox_texture.jpg");
            _map = new Map();
            _swordTrace = new SwordPath();
            _textShape = new Text("Just0neMMORPG", 25, 125, 180, 0);

            for (var i = 0; i < 3; i++)
            {
                var testRune = new RuneCircle(
                    new Layer(
                        new Offset(75),
                        new ConvexPolygon(7),
                        new Star(7, 3, new Vector4(255, 170, 0, 255)),
                        new Runes(7),
                        new Circle(new Vector4(255, 0, 255, 255))
                    ),
                    new Layer(
                        new Offset(67),
                        new ConvexPolygon(7),
                        new Star(7, 2, new Vector4(170, 170, 255, 255)),
                        new Runes(14)
                    ),
                    new Layer(
                        new Offset(57),
                        new ConvexPolygon(7),
                        new Star(7, 2, new Vector4(0, 255, 170, 255)),
                        new Star(21, 2, new Vector4(100, 100, 255, 255)),
                        new ConvexPolygon(21, new Vector4(255, 100, 100, 255)),
                        new Runes(21),
                        new ConvexPolygon(21, new Vector4(255, 100, 100, 255))
                    ));
                testRune.Position = new Vector3((float) Random.NextDouble(), (float) Random.NextDouble(),
                    (float) Random.NextDouble()) * 10f;
                testRune.Rotation = new Vector3((float) Random.NextDouble(), (float) Random.NextDouble(),
                    (float) Random.NextDouble()) * 2 * MathF.PI;

                testRune.Rebuild();
                _testRunes.Add(testRune);
            }

            _cursor = Drawable.Create(0, 0, 0, 10, 10);
            _cursor.Texture = new Texture("assets/textures/сursor.png");

            ImguiContext = new ImguiRenderer();

            //ImGui.NewFrame();
        }

        protected override unsafe void OnRenderFrame(FrameEventArgs e)
        {
            _data.DeltaTime = (float) e.Time;
            _data.Width = Width;
            _data.Height = Height;
            
            ImguiContext.SetPerFrameImGuiData(_data);

            //_frameBuffer.Use();
            FrameBuffer.Clear();

            GlobalTime += e.Time;

            if (GlobalTime - LastFpsCheck > 1)
            {
                Fps = (int) FramesCount;
                LastFpsCheck = (float) GlobalTime;
                FramesCount = 0;

                FpsValues[^1] = Fps;
                
                for (int i = 1; i < FpsValues.Length; ++i)
                {
                    FpsValues[i - 1] = FpsValues[i];
                }

                
            }
            else
            {
                FpsValues[^1] = FramesCount;
                FramesCount++;
            }


            var input = Keyboard.GetState();

            if (input.IsKeyDown(Key.Escape))
                Exit();
            if (input.IsKeyDown(Key.LControl))
                GameFocused = false;
            if (input.IsKeyDown(Key.RControl))
                GameFocused = true;
            if (input.IsKeyDown(Key.Q))
            {
                foreach (var rc in _testRunes)
                {
                    rc.StartTime = 0;
                }
            }

            if (input.IsKeyDown(Key.K))
                AutoWalkState = !AutoWalkState;

            if (AutoWalkState)
            {
                _camera.Position += _camera.Front * (float)e.Time * 10f;
            }

            if (GameFocused)
            {
                _camera.KeyHandler(input, e.Time);
                _camera.Update(Mouse.GetState().X, Mouse.GetState().Y);
            }

            //_map.Update(_camera);
            //_camera.LookAt = new Vector3(0);
            //_camera.Position = new Vector3(0, 0, 2);


            Title = "Sword Attack > fps: " + Fps + ", Pos: " + _camera.Position;


            _data.View = _camera.GetViewMatrix();
            _data.CameraFront = _camera.Front;
            _data.CameraFov = _camera.Fov;
            _data.ViewWithoutPosition = _camera.GetViewRotateMatrix();
            _data.CameraPosition = _camera.Position;
            _data.GlobalTime = (float) GlobalTime;

            _data.Shader = Assets.Shaders["default"];
            _data.Projection = _data.Perspective;

            _swordTrace.Update(this);
            _swordTrace.Draw(_data);
            
            //_map.Draw(_data);

            
            //GL.DepthFunc(DepthFunction.Always);
            //GL.Disable(EnableCap.DepthTest);
            var spell = "futarkus";
            foreach (var rune in _testRunes)
            {
                //rune.Shape.AnimationRotate = new Vector3(0, 0, rune.Shape.AnimationRotate.Z + MathF.PI / 4 * (float)e.Time);
                //rune.Shape.Origin = new Vector3(0.5f, 0.5f, 0);
                rune.Draw(_data);
            }
            //GL.Enable(EnableCap.DepthTest);


            if (GameCursorVisible && !_swordTrace.Focused)
            {
                _data.Shader = Assets.Shaders["cursor"];
                _data.Projection = _data.Orthogonal;
                _cursor.Position = new Vector3(Mouse.GetCursorState().X - X, Mouse.GetCursorState().Y - Y, -1);
                //_cursor.Draw(_data);
            }

            //GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Line);
            _data.Shader = Assets.Shaders["skybox"];
            _data.Projection = _data.Perspective;
            _skybox.Draw(_data); 
            //GL.PolygonMode(MaterialFace.FrontAndBack, PolygonMode.Fill);

            /*
             * ImGui window manage
             */
            {
                if(!DarkTheme)
                    ImGui.StyleColorsLight();
                else ImGui.StyleColorsDark();

                ImGui.NewFrame();
                byte[] evalText = new byte[1024];
                ImGui.Begin("Lua Console");
                //ImGui.LogText(ConsoleLog);
                ImGui.InputTextMultiline("l", ref ConsoleLog, 10000, new Vector2(200, 70));
                ImGui.InputText("", evalText, 1024);
                
                if (ImGui.Button("Eval"))
                {
                    //ConsoleLog += System.Text.Encoding.Default.GetString(evalText)+"\n";
                    //evalText = new byte[1024];
                }
                ImGui.End();

                

                bool test = false;
                ImGui.Begin("Hello, world!"); // Create a window called "Hello, world!" and append into it.
                ImGui.PlotLines("fps", ref FpsValues[0], FpsValues.Length);
                ImGui.Checkbox("Focus", ref GameFocused);
                ImGui.Checkbox("Dark Theme", ref DarkTheme);
                ImGui.End();
            }

            ImguiContext.Draw(_data);
            
            
            //FrameBuffer.Unbind();
            //FrameBuffer.Clear();

            _data.Shader = Assets.Shaders["blur"];
            //var bloomResult = _frameBuffer.Apply(_data, 1);

            _data.Projection = _data.Orthogonal;
            _data.Shader = Assets.Shaders["mix"];
            _data.Shader.SetFloat("exposure", 5f);
            _data.Shader.SetInt("bloomBlur", 1);

            //bloomResult.Use(1);
            //_frameShape.Texture = _frameBuffer.GetTexture();
            //_frameShape.Draw(_data);
            

            //TextShape.Draw(Data);
            
            //ImGui.CreateContext();
            
            


            Context.SwapBuffers();
            base.OnRenderFrame(e);
        }


        protected override void OnKeyPress(OpenTK.KeyPressEventArgs e)
        {
            ImguiContext.OnKeyPress(e.KeyChar);
        }

        /*protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
        }*/

        protected override void OnKeyDown(KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.AltLeft)
                _swordTrace.Focused = true;

            base.OnKeyDown(e);
        }

        protected override void OnKeyUp(KeyboardKeyEventArgs e)
        {
            if (e.Key == Key.AltLeft)
                _swordTrace.Focused = false;

            base.OnKeyDown(e);
        }

        protected override void OnMouseMove(MouseMoveEventArgs e)
        {
            
            if (Focused && GameFocused)
            {
                CursorVisible = false;
                
                Helper.ClipCursor(new Rect(X, Y, X + Width, Y + Height));
                
                //Console.WriteLine((e.X - (Width / 2f))+" "+(e.XDelta));
                Mouse.SetPosition(X + Width / 2f, Y + Height / 2f);
            }
            else
            {
                ImguiContext.OnMouseMove(new Vector2(e.X, e.Y));
                
                Helper.ClipCursor(null);
                CursorVisible = true;
            }

            base.OnMouseMove(e);
        }

        private void UpdateMatrix()
        {
            _data.Orthogonal = Matrix4.CreateOrthographicOffCenter(0.0f, Width, Height, 0.0f, 0.1f, 100.0f);
            _data.Perspective = Matrix4.CreatePerspectiveFieldOfView(MathHelper.DegreesToRadians(90.0f),
                Width / (float) Height, 0.1f, 200.0f);
        }

        private void UpdateBuffers()
        {
            _frameBuffer = new FrameBuffer(Width, Height);
            _frameShape = Drawable.Create2d(0, 0, -1, Width, Height, DrawableUv.FlipVerticaly);
        }

        protected override void OnResize(EventArgs e)
        {
            GL.Viewport(0, 0, Width, Height);

            UpdateMatrix();
            UpdateBuffers();

            base.OnResize(e);
        }
    }

    public struct Rgba24
    {
    }
}