﻿#version 330

out vec4 outputColor;

in vec2 texCoord;
in vec3 FragPos;

uniform sampler2D texture0;
uniform float global_time;
uniform vec3 shape_position;
uniform vec3 shape_size;

void main()
{
    
    vec2 pos = gl_FragCoord.xy - shape_position.xy;

    outputColor = vec4(0);

    float border = 2;

    if((pos.y < border - shape_size.y || pos.y > shape_size.y - border) || (pos.x < border - shape_size.x  || pos.x > shape_size.x - border)){
        outputColor = vec4(1.0, 1.0, 0.0, 1);
     }
    


    float gamma = 0.8;
    outputColor.rgb = pow(outputColor.rgb, vec3(1.0/gamma));
}