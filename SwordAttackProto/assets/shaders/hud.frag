#version 330

out vec4 outputColor;

in vec2 texCoord;
in vec4 color;

uniform sampler2D texture0;
uniform float global_time;

void main()
{
    
    vec4 resColor = color*texture(texture0, texCoord);

    //if(texture0 == -1)
    //    color = vec4(0.2, 0.5, 0.2, 1);

    outputColor = resColor;


    float gamma = 0.8;
    outputColor.rgb = pow(outputColor.rgb, vec3(1.0/gamma));
}