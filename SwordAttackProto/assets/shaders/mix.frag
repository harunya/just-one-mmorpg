﻿#version 330 core
out vec4 FragColor;
  
in vec2 TexCoords;

uniform sampler2D scene;
uniform sampler2D bloomBlur;
uniform float exposure;

void main()
{             
    const float gamma = 2.2;
    vec4 hdrColor = texture(scene, TexCoords).rgba;      
    vec4 bloomColor = texture(bloomBlur, TexCoords).rgba;
    hdrColor += bloomColor; // additive blending

    vec4 result = vec4(1.0) - exp(-hdrColor * exposure);

    //result = pow(result, vec3(1.0 / gamma));
    FragColor = result;
}