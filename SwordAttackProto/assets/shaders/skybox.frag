#version 330
#extension GL_ARB_separate_shader_objects : enable


in vec2 texCoord;
in vec3 Normal;
in vec3 FragPos;

uniform sampler2D texture0;
uniform float global_time;
uniform vec3 viewPos;
uniform vec3 lightPos;
uniform vec3 lightColor;


layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;  

void main()
{
    vec4 color = texture(texture0, texCoord);
    FragColor = color;

    //float gamma = 0.8;
    //FragColor.rgb = pow(FragColor.rgb, vec3(1.0/gamma));

    //FragColor.xyz = FragColor.xyz * texture2D(texture0, texCoord);
}