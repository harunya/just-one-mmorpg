﻿layout(points) in;
layout(triangles, max_vertices = 6) out;

in vs_output
{
    vec4 color;
} gs_in[];

out gs_output
{
    vec2 st;
    vec4 color;
} gs_out;

void main()
{
    gl_Position  = gl_in[0].gl_Position;
    gs_out.st    = vec2(0.0f, 0.0f);
    gs_out.color = gs_in[0].color;
    EmitVertex();

    gl_Position  = gl_in[1].gl_Position;
    gs_out.st    = vec2(0.0f, 1.0f);
    gs_out.color = gs_in[1].color;
    EmitVertex();

    gl_Position  = gl_in[2].gl_Position;
    gs_out.st    = vec2(1.0f, 0.0f);
    gs_out.color = gs_in[2].color;
    EmitVertex();

    gl_Position  = gl_in[3].gl_Position;
    gs_out.st    = vec2(1.0f, 1.0f);
    gs_out.color = gs_in[3].color;
    EmitVertex();

    EndPrimitive();
}