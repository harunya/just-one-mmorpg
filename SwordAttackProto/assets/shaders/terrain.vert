#version 330 core

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec2 aTexCoord;
layout (location = 2) in vec3 aNormal;

out vec2 TexCoord_CS_in;
out vec3 Normal_CS_in;
out vec3 WorldPos_CS_in;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 screen_size;
uniform float global_time;

void main(void)
{
    Normal_CS_in = (model * vec4(aNormal, 0.0)).xyz;
    TexCoord_CS_in = aTexCoord;
    WorldPos_CS_in = (model * vec4(aPosition, 1.0)).xyz;
    //gl_Position = projection * view * model * vec4(aPosition, 1.0);
}