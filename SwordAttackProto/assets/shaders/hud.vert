#version 330 core

layout(location = 0) in vec3 aPosition;

layout(location = 1) in vec2 aTexCoord;
layout(location = 3) in vec4 aColor;

out vec2 texCoord;
out vec4 color;

uniform mat4 projection;
uniform mat4 model;
uniform mat4 view;
uniform vec3 screen_size;
uniform float origin;



void main(void)
{
    texCoord = aTexCoord;
    color = aColor;
    gl_Position = projection*vec4(aPosition.xy, -1.0, 1.0);
}