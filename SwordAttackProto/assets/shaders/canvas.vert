#version 330 core

layout(location = 0) in vec2 aPosition;

out vec3 pos;

uniform mat4 projection;
uniform mat4 model;

void main()
{
    gl_Position = projection * model * vec4(aPosition, -1.0, 1.0);
    pos = gl_Position.xyz;
}