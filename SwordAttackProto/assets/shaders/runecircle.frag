﻿#version 330 core
#extension GL_ARB_separate_shader_objects : enable


layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec2 aTexCoord;

in vec2 texCoord;
in vec3 Normal;
in vec3 FragPos;

uniform sampler2D texture0;
uniform float global_time;
uniform vec3 viewPos;
uniform vec3 color;
uniform vec3 lightPos;
uniform vec3 lightColor;


layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;  


void main()
{
    vec2 tc = texCoord;
    vec2 p = -1.0 + 2.0 * tc;
    float len = length(p);
    vec2 uv = tc + (p/len)*cos(len*6.0-global_time)*0.05;
    //vec4 inputColor = texture2D(texture0,uv);

    vec4 inputColor = texture(texture0, texCoord);
    FragColor = inputColor*vec4(color, 1.0);

    //float gamma = 0.8;
    //FragColor.rgb = pow(FragColor.rgb, vec3(1.0/gamma));

    //float brightness = dot(FragColor.rgb, vec3(0.2126, 0.7152, 0.0722));
    //if(brightness > 0.001)
    BrightColor = FragColor.rgba;
    //else
    //    BrightColor = vec4(0.0, 0.0, 0.0, 1.0);
}