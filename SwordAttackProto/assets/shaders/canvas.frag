#version 330

out vec4 outputColor;

in vec3 pos;

uniform float global_time;
uniform vec2 window_size;

void main()
{
    outputColor = vec4((pos.x+1)/2, (pos.y+1)/2, 0, 1);

    //float gamma = 0.8;
    //outputColor.rgb = pow(outputColor.rgb, vec3(1.0/gamma));
}