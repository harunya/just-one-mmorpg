#version 330
#extension GL_ARB_separate_shader_objects : enable


in vec2 texCoord;
in vec3 Normal;
in vec3 FragPos;

uniform sampler2D texture0;
uniform float global_time;
uniform vec3 viewPos;
uniform vec3 lightPos;
uniform vec3 lightColor;


layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;  

void main()
{
    vec4 color = texture(texture0, texCoord);
    FragColor = color;

    float gamma = 0.8;
    FragColor.rgb = pow(FragColor.rgb, vec3(1.0/gamma));


    float ambient = 0.1;
    float specularStrength = 0.4;
    
    vec3 norm = normalize(Normal);
    vec3 lightDir = normalize(lightPos - FragPos);  

    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * lightColor;


    BrightColor = vec4(0);

    //FragColor.xyz = FragColor.xyz * (ambient + diffuse + specular);
}