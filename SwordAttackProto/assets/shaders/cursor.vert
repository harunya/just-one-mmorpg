#version 330 core

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec2 aTexCoord;
layout (location = 2) in vec3 aNormal;

out vec2 texCoord;
out vec3 Normal;
out vec3 FragPos;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;
uniform vec3 screen_size;
uniform float global_time;

void main(void)
{
    Normal = aNormal;
    texCoord = aTexCoord;
    FragPos = vec3(model * vec4(aPosition, 1.0));
    gl_Position = projection * model * vec4(aPosition, 1.0);
}