﻿#version 330 core

out vec4 FragColor;
  
in vec2 TexCoords;

uniform bool horizontal;
uniform float global_time;
uniform float sigma;     // The sigma value for the gaussian function: higher value means more blur
                         // A good value for 9x9 is around 3 to 5
                         // A good value for 7x7 is around 2.5 to 4
                         // A good value for 5x5 is around 2 to 3.5
                         // ... play around with this based on what you need :)

uniform float blurSize;  // This should usually be equal to
                         // 1.0f / texture_pixel_width for a horizontal blur, and
                         // 1.0f / texture_pixel_height for a vertical blur.

uniform sampler2D image;  // Texture that will be blurred by this shader

const float pi = 3.14159265f;


void main() {
    float blurSize;
    float numBlurPixelsPerSide = 8.0f;
    vec2 blurMultiplyVec;
    if(horizontal){
        blurMultiplyVec = vec2(1.0f, 0.0f);
        blurSize = 1.0f / textureSize(image, 0).x;
    }else{
        blurMultiplyVec = vec2(0.0f, 1.0f);
        blurSize = 1.0f / textureSize(image, 0).y;
    }

    float sigma = 5;

    // Incremental Gaussian Coefficent Calculation (See GPU Gems 3 pp. 877 - 889)
    vec3 incrementalGaussian;
    incrementalGaussian.x = 1.0f / (sqrt(2.0f * pi) * sigma);
    incrementalGaussian.y = exp(-0.5 / (sigma * sigma)); // -0.5 !!!! 
    incrementalGaussian.z = incrementalGaussian.y * incrementalGaussian.y;

    vec4 avgValue = vec4(0.0f, 0.0f, 0.0f, 0.0f);
    float coefficientSum = 0.0f;

    // Take the central sample first...
    avgValue += texture2D(image,TexCoords) * incrementalGaussian.x;
    coefficientSum += incrementalGaussian.x;
    incrementalGaussian.xy *= incrementalGaussian.yz;

    // Go through the remaining 8 vertical samples (4 on each side of the center)
    for (float i = 1.0f; i <= numBlurPixelsPerSide; i++) { 
        avgValue += texture2D(image, TexCoords - i * blurSize * 
                                blurMultiplyVec) * incrementalGaussian.x;         
        avgValue += texture2D(image, TexCoords + i * blurSize * 
                                blurMultiplyVec) * incrementalGaussian.x;         
        coefficientSum += 2 * incrementalGaussian.x;
        incrementalGaussian.xy *= incrementalGaussian.yz;
    }

    FragColor = avgValue / coefficientSum;
}