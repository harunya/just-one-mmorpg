#version 330 core

layout(location = 0) in vec3 aPosition;
layout(location = 1) in vec2 aTexCoord;
layout (location = 2) in vec3 aNormal;

out vec2 texCoord;
out vec3 Normal;
out vec3 FragPos;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 viewWithoutPosition;
uniform mat4 model;
uniform vec3 screen_size;
uniform float global_time;

void main(void)
{
    Normal = aNormal;
    texCoord = aTexCoord;
    FragPos = vec3(aPosition);
    gl_Position = projection * viewWithoutPosition * vec4(aPosition*100, 1.0);

    //gl_Position.z = (1 - gl_Position.z / gl_Position.w) * gl_Position.w;
}