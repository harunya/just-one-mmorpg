#version 330
#extension GL_ARB_separate_shader_objects : enable


in vec2 TexCoord_FS_in;
in vec3 Normal_FS_in;
in vec3 WorldPos_FS_in;

uniform sampler2D texture0;
uniform float global_time;
uniform vec3 viewPos;
//uniform vec3 lightPos;
//uniform vec3 lightColor;


layout (location = 0) out vec4 FragColor;
layout (location = 1) out vec4 BrightColor;  

void main()
{
    vec4 color = texture(texture0, TexCoord_FS_in);
    FragColor = vec4(1) + color*0;

    float gamma = 0.8;
    FragColor.rgb = pow(FragColor.rgb, vec3(1.0/gamma));

    vec3 lightPos = vec3(-3,10,0);
    vec3 lightColor = vec3(1,1,1);
    float ambient = 0.1;
    float specularStrength = 0.3;
    
    vec3 norm = normalize(Normal_FS_in);
    vec3 lightDir = normalize(lightPos - WorldPos_FS_in);  

    float diff = max(dot(norm, lightDir), 0.0);
    vec3 diffuse = diff * lightColor;

    vec3 viewDir = normalize(viewPos - WorldPos_FS_in);
    vec3 reflectDir = reflect(-lightDir, norm);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
    vec3 specular = specularStrength * spec * lightColor;


    BrightColor = vec4(0);

    FragColor.xyz = FragColor.xyz * (ambient + diffuse + specular);
    FragColor.w = 1;
}