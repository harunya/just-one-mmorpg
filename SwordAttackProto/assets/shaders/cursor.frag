﻿#version 330

out vec4 outputColor;

in vec2 texCoord;
in vec3 pos;

uniform float global_time;
uniform vec3 shape_position;
uniform vec3 shape_size;
uniform vec2 window_size;
uniform sampler2D texture0;

void main()
{
    vec2 pos = (gl_FragCoord.xy - shape_position.xy)/shape_size.xy;

    outputColor = texture(texture0, texCoord) * vec4((1.0+cos(global_time))/2.0, (1.0+sin(global_time))/2.0, (1.0+tan(global_time))/2.0, 1);

    //float gamma = 0.8;
    //outputColor.rgb = pow(outputColor.rgb, vec3(1.0/gamma));
}