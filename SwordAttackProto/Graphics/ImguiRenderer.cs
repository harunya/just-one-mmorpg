﻿using System;
using System.Numerics;
using System.Runtime.InteropServices;
using ImGuiNET;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using SwordAttackProto.code_asbest.Common;
using SwordAttackProto.code_asbest.Graphics;

namespace SwordAttackProto.Graphics
{
    public class ImguiRenderer
    {
        private IntPtr _ptr;
        private Texture Atlas;
        
        public ImguiRenderer()
        {
            _ptr = ImGui.CreateContext();
            Bind();
            
            ImGui.GetIO().Fonts.AddFontDefault();
            
            ImGuiIOPtr io = ImGui.GetIO();
            
            // Build
            IntPtr pixels;
            int width, height, bytesPerPixel;
            io.Fonts.GetTexDataAsRGBA32(out pixels, out width, out height, out bytesPerPixel);

            var bytes = new byte[width*height*bytesPerPixel];
            Marshal.Copy(pixels, bytes, 0, bytes.Length);
            Atlas = new Texture(bytes, width, height);

            io.Fonts.SetTexID(new IntPtr(Atlas.Id));

            SetKeyMappings();
        }

        public void Bind()
        {
            ImGui.SetCurrentContext(_ptr);
        }
        public void SetPerFrameImGuiData(DrawingData data)
        {
            Bind();
            
            var scaleFactor = new Vector2(1);
            var io = ImGui.GetIO();
            io.DisplaySize = new Vector2(
                data.Width / scaleFactor.X,
                data.Height / scaleFactor.Y);
            io.DisplayFramebufferScale = scaleFactor;
            io.DeltaTime = data.DeltaTime; // DeltaTime is in seconds.

            io.MouseDown[0] = Mouse.GetState().LeftButton == ButtonState.Pressed;
            io.MouseDown[1] = Mouse.GetState().RightButton == ButtonState.Pressed;
        }

        public void OnKeyPress(char keyChar)
        {
            Bind();
            
            var io = ImGui.GetIO();
            io.AddInputCharacterUTF16(keyChar);
        }
        
        private static void SetKeyMappings()
        {
            ImGuiIOPtr io = ImGui.GetIO();
            io.KeyMap[(int)ImGuiKey.Tab] = (int)Key.Tab;
            io.KeyMap[(int)ImGuiKey.LeftArrow] = (int)Key.Left;
            io.KeyMap[(int)ImGuiKey.RightArrow] = (int)Key.Right;
            io.KeyMap[(int)ImGuiKey.UpArrow] = (int)Key.Up;
            io.KeyMap[(int)ImGuiKey.DownArrow] = (int)Key.Down;
            io.KeyMap[(int)ImGuiKey.PageUp] = (int)Key.PageUp;
            io.KeyMap[(int)ImGuiKey.PageDown] = (int)Key.PageDown;
            io.KeyMap[(int)ImGuiKey.Home] = (int)Key.Home;
            io.KeyMap[(int)ImGuiKey.End] = (int)Key.End;
            io.KeyMap[(int)ImGuiKey.Delete] = (int)Key.Delete;
            io.KeyMap[(int)ImGuiKey.Backspace] = (int)Key.BackSpace;
            io.KeyMap[(int)ImGuiKey.Enter] = (int)Key.Enter;
            io.KeyMap[(int)ImGuiKey.Escape] = (int)Key.Escape;
            io.KeyMap[(int)ImGuiKey.Space] = (int)Key.Space;
            io.KeyMap[(int)ImGuiKey.A] = (int)Key.A;
            io.KeyMap[(int)ImGuiKey.C] = (int)Key.C;
            io.KeyMap[(int)ImGuiKey.V] = (int)Key.V;
            io.KeyMap[(int)ImGuiKey.X] = (int)Key.X;
            io.KeyMap[(int)ImGuiKey.Y] = (int)Key.Y;
            io.KeyMap[(int)ImGuiKey.Z] = (int)Key.Z;
        }

        public unsafe void Draw(DrawingData data)
        {
            Bind();
            
            // Render dear imgui into screen
            ImGui.Render();
            
            
            GL.Enable(EnableCap.ScissorTest);
            GL.DepthFunc(DepthFunction.Always);
            
            int vbo = GL.GenBuffer();
            int ebo = GL.GenBuffer();
            int vao = GL.GenVertexArray();
            
            var p = ImGuiNative.igGetDrawData();

            ImDrawList** drawList = (ImDrawList**) p->CmdLists;
            for (int i = 0; i < p->CmdListsCount; i++)
            {
                var cmDrawList = *drawList[i];

                
                GL.BindVertexArray(vao);
                
                GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
                GL.BufferData(BufferTarget.ArrayBuffer, cmDrawList.VtxBuffer.Size * sizeof(ImDrawVert), cmDrawList.VtxBuffer.Data, BufferUsageHint.StreamDraw);
                
                GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
                GL.BufferData(BufferTarget.ElementArrayBuffer, cmDrawList.IdxBuffer.Size * sizeof(short), cmDrawList.IdxBuffer.Data, BufferUsageHint.StreamDraw);
                
                GL.VertexAttribPointer(0,   2, VertexAttribPointerType.Float, false, sizeof(ImDrawVert), 0);
                GL.VertexAttribPointer(1,    2, VertexAttribPointerType.Float, false, sizeof(ImDrawVert), 2*sizeof(float));
                GL.VertexAttribPointer(3, 4, VertexAttribPointerType.UnsignedByte, true,  sizeof(ImDrawVert), 4*sizeof(float));
                GL.EnableVertexAttribArray(0);
                GL.EnableVertexAttribArray(1);
                GL.EnableVertexAttribArray(3);

                
                ImDrawCmd* drawCmd = (ImDrawCmd*)cmDrawList.CmdBuffer.Data.ToPointer();
                for (uint j = 0; j < (uint)cmDrawList.CmdBuffer.Size; ++j)
                {
                    var pcmd = drawCmd[j];

                    //var im = GetImageResourceSet(cmd.TextureId);
                    GL.Scissor((int)pcmd.ClipRect.X, (int)(data.Height - pcmd.ClipRect.W), (int)(pcmd.ClipRect.Z - pcmd.ClipRect.X), (int)(pcmd.ClipRect.W - pcmd.ClipRect.Y));
                    //GL.BindTexture(TextureTarget.Texture2D, pcmd.TextureId.ToInt32())
                    data.Shader = Assets.Shaders["HUD"];
                    data.Projection = data.Orthogonal;
                    
                    GL.BindVertexArray(vao);
                    GL.BindBuffer(BufferTarget.ArrayBuffer, vbo);
                    GL.BindBuffer(BufferTarget.ElementArrayBuffer, ebo);
                    Drawable.SetCapabilitiesForDrawCall(data, Atlas, OpenTK.Vector3.Zero, OpenTK.Vector3.Zero, OpenTK.Vector3.Zero, OpenTK.Vector3.One);
                    GL.DrawElements(PrimitiveType.Triangles, (int)pcmd.ElemCount, DrawElementsType.UnsignedShort, (int)(pcmd.IdxOffset * sizeof(short))); // (int)(pcmd.IdxOffset * sizeof(int))
                }
            }

            GL.Disable(EnableCap.ScissorTest);
            GL.DepthFunc(DepthFunction.Lequal);
        }

        public void OnMouseMove(Vector2 vector2)
        {
            Bind();
            
            var io = ImGui.GetIO();
            io.MousePos = vector2;
        }
    }
}