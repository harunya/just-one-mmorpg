﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using OpenTK.Graphics.OpenGL;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Advanced;
using SixLabors.ImageSharp.PixelFormats;
using SwordAttackProto.code_asbest.Common;
using SwordAttackProto.code_asbest.Gameplay;
using SwordAttackProto.code_asbest.Graphics;

namespace SwordAttackProto.gameplay
{
    public class Map
    {
        private Drawable[,] _objs;
        private int chunkSize = 100;

        public Map()
        {
            Load("test.map");
        }

        private void Load(string path)
        {
            var bytes = File.ReadAllBytes(path);
            var width = BitConverter.ToInt32(bytes, 8);
            var height = BitConverter.ToInt32(bytes, 12);

            _objs = new Drawable[height / chunkSize, width / chunkSize];

            float[] vertices = new float[chunkSize * chunkSize * 6 * 8];
            int ptr;

            void CreateVertex(Vector3 a, Vector2 uv, Vector3 normal)
            {
                //Pos
                vertices[ptr++] = a.X;
                vertices[ptr++] = a.Y;
                vertices[ptr++] = a.Z;
                //uv
                vertices[ptr++] = uv.X;
                vertices[ptr++] = uv.Y;
                //normal
                vertices[ptr++] = normal.X;
                vertices[ptr++] = normal.Y;
                vertices[ptr++] = normal.Z;
            }

            void CreateTriangle(Vector3 a, Vector3 b, Vector3 c)
            {
                var m1M2 = b - a;
                var m1M3 = c - a;
                var normal = new Vector3(m1M2.Y*m1M3.Z - m1M3.Y*m1M2.Z, -(m1M3.X*m1M2.Z - m1M2.X*m1M3.Z), m1M2.X*m1M3.Y - m1M3.X*m1M2.Y);
                normal = Vector3.Normalize(normal);
                normal = Vector3.Negate(normal);
                
                CreateVertex(a, Vector2.Zero, normal);
                CreateVertex(b, Vector2.Zero, normal);
                CreateVertex(c, Vector2.Zero, normal);
            }

            var generator = new Perlin2D();
            float scale = 2;

            for (int cy = 0; cy < _objs.GetLength(0); ++cy)
            {
                for (int cx = 0; cx < _objs.GetLength(1); ++cx)
                {
                    ptr = 0;
                    vertices = new float[chunkSize * chunkSize * 6 * 8];
                    for (int y = cy*chunkSize; y < cy*chunkSize + chunkSize-1; ++y)
                    {
                        for (int x = cx*chunkSize; x < cx*chunkSize + chunkSize-1; ++x)
                        {
                            
                            var a00 = new Vector3(x, bytes[0x100 + y * width + x]/255f * scale, y)/scale;
                            var a01 = new Vector3(x+1, bytes[0x100 + y * width + x + 1]/255f* scale, y)/scale;
                            var a10 = new Vector3(x, bytes[0x100 + (y + 1) * width + x]/255f* scale, y+1)/scale;
                            var a11 = new Vector3(x+1, bytes[0x100 + (y + 1) * width + x + 1]/255f* scale, y+1)/scale;
                            // var a00 = new Vector3(x, generator.Noise(x, y), y);
                            // var a01 = new Vector3(x+1, generator.Noise(x+1, y), y);
                            // var a10 = new Vector3(x, generator.Noise(x, y+1), y+1);
                            // var a11 = new Vector3(x+1, generator.Noise(x+1, y+1), y+1);

                            CreateTriangle(a00, a10, a11);
                            CreateTriangle(a00, a11, a01);
                        }
                    }

                    var obj = new Drawable();
                    obj.Fill(vertices, new[]
                    {
                        new VertexAttribute(0, 3, 8, 0),
                        new VertexAttribute(1, 2, 8, 3),
                        new VertexAttribute(2, 3, 8, 5),
                    });
                    _objs[cy, cx] = obj;
                }
            }
        }

        public void Draw(DrawingData data)
        {
            //GL.Enable(EnableCap.PolygonSmooth );
            GL.PatchParameter(PatchParameterInt.PatchVertices, 3);
            //GL.PatchParameter(PatchParameterInt.PatchVertices, 3);
            //GL.PatchParameter(PatchParameterFloat.PatchDefaultOuterLevel, new []{3f, 3f, 3f});
            //GL.PatchParameter(PatchParameterFloat.PatchDefaultInnerLevel, new []{1f});
            data.Shader = Assets.Shaders["terrain"];
            data.Shader.SetFloat("gTessellationLevel", 500f);
            for (int y = 0; y < _objs.GetLength(0); y += 1)
            {
                for (int x = 0; x < _objs.GetLength(1); x += 1)
                {
                    var _object = _objs[y, x];
                    
                    if(OpenTK.Vector3.CalculateAngle(new OpenTK.Vector3(x, 0, y)*chunkSize-data.CameraPosition, data.CameraFront) > data.CameraFov/2)
                        continue;
                    
                    _object.DrawType = PrimitiveType.Patches;
                    _object.Draw(data);
                }
            }
        }
    }
}