﻿using System;
using System.Collections.Generic;
using OpenTK;
using OpenTK.Input;
using SwordAttackProto.code_asbest.Common;
using SwordAttackProto.code_asbest.Graphics;
using GameWindow = OpenTK.GameWindow;

namespace SwordAttackProto.gameplay
{
    internal class SwordPath
    {
        private readonly Canvas _traces = new Canvas();
        private readonly List<Vector4> _tracesCoords = new List<Vector4>();
        private readonly Drawable _sword;
        private readonly Drawable _traceArea;

        private List<Vector4> _applyTracesCoords = new List<Vector4>();
        private const int CurrentTrace = 0;

        private Vector4 _lastCoord;
        private Vector2 _cursorPosition;


        public bool Focused { get; set; }
        private bool _blockPressed;


        public SwordPath()
        {
            _sword = Drawable.Create3d(0, 0, 0, 0.02f, 0.5f, 0.001f);
            _sword.Texture = new Texture("assets/textures/test.png");

            _traceArea = Drawable.Create(0, 0, 0, 1f, 1f);
            //TraceArea.Texture = new Texture("assets/textures/swordtrace_area.png");
            //TraceArea.Origin = new Vector3(1f, 1f, 0);
        }

        public void Update(GameWindow win)
        {
            //Sword.Origin = new Vector3(0.5f, 0.7f, 0);
            //Sword.Scale = new Vector3(1/800, );
            var pos = new Vector2(win.Width* MathF.Abs(MathF.Cos((float)Program.GlobalTime/2)), 200);
            var g = 10f*MathF.Abs(MathF.Sin((float)Program.GlobalTime));

            var h = win.Width / (2 * MathF.Tan(MathF.PI / 4));
            var tgA = (pos.X - win.Width / 2) /h;
            var rX = g * tgA;


            var h2 = win.Height / (2 * MathF.Tan(MathF.PI / 4));
            var tgA2 = (pos.Y - win.Height / 2) / h2;
            var rY = g * tgA2;

            _sword.Position = new Vector3(rX, rY, -g);
            //Sword.Rotation = new Vector3(-MathF.Abs(MathF.Cos((float)Program.GlobalTime * 10)) * MathF.PI / 2, MathF.PI / 2, 0);


            if (!Focused)
            {
                if(_tracesCoords.Count > 0)
                {
                    _applyTracesCoords = _tracesCoords;
                    _tracesCoords.Clear();
                }

                return;
            }


            var state = Mouse.GetCursorState();

            // If mouse escape from area
            var leftBorder = win.X + _traceArea.Position.X - _traceArea.Scale.X;
            var rightBorder = win.X + _traceArea.Position.X + _traceArea.Scale.X;
            var upBorder = win.Y + _traceArea.Position.Y - _traceArea.Scale.Y;
            var downBorder = win.Y + _traceArea.Position.Y + _traceArea.Scale.Y;

            _cursorPosition = new Vector2(state.X, state.Y);

            if (state.X < leftBorder || state.X > rightBorder)
            {
                _cursorPosition.X = win.X + _traceArea.Position.X - state.X > 0 ? leftBorder : rightBorder;
            }
            if (state.Y < upBorder || state.Y > downBorder)
            {
                _cursorPosition.Y = win.Y + _traceArea.Position.Y - state.Y > 0 ? upBorder : downBorder;
            }

            if (_cursorPosition.X != state.X || _cursorPosition.Y != state.Y)
            {
                Mouse.SetPosition(_cursorPosition.X, _cursorPosition.Y);
            }

            
            _traceArea.Scale = new Vector3(win.Width / 6, win.Height / 6, 0);
            _traceArea.Position = new Vector3(win.Width / 2, win.Height / 2, -1);

            _lastCoord = _tracesCoords.Count > 0 ? _tracesCoords[_tracesCoords.Count - 1] : new Vector4(_traceArea.Position.X, _traceArea.Position.Y, _traceArea.Position.X + _traceArea.Scale.X, _traceArea.Position.Y + _traceArea.Scale.Y);


            _traces.Clear();

            foreach (var coord in _tracesCoords)
            {
                _traces.Add(new Line(coord.X, coord.Y, coord.Z, coord.W));
            }


            _traces.Add(new Line(_lastCoord.Z, _lastCoord.W, _cursorPosition.X - win.X, _cursorPosition.Y - win.Y));


            if (state.LeftButton.HasFlag(ButtonState.Pressed))
            {
                if (!_blockPressed)
                    DrawByMouse(win);

                _blockPressed = true;
            }
            else if(state.LeftButton.HasFlag(ButtonState.Released))
            {
                _blockPressed = false;
            }

        }

        private Vector2 GetNextTracePoint()
        {

            return Helper.Linearin(_applyTracesCoords[CurrentTrace].Xy, _applyTracesCoords[CurrentTrace+1].Xy, 0);
        }

        public void Draw(DrawingData data)
        {
            data.Shader = Assets.Shaders["sword"];
            data.Projection = data.Perspective;
            _sword.Draw(data);

            if (!Focused)
                return;


            data.Shader = Assets.Shaders["canvas"];
            data.Projection = data.Orthogonal;
            _traces.Draw(data);

            data.Shader = Assets.Shaders["sword_tracearea"];
            data.Projection = data.Orthogonal;
            _traceArea.Draw(data);

            
        }

        public void DrawByMouse(GameWindow win)
        {
            _tracesCoords.Add(new Vector4(_lastCoord.Z, _lastCoord.W, _cursorPosition.X - win.X, _cursorPosition.Y - win.Y));
        }
    }
}
